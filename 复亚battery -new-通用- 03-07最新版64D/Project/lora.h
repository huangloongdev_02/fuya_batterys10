#ifndef _H_LORA_ 
#define _H_LORA_

#include "source.h"
#define DEVICE_HAS_GPS 0
extern uint8_t report_has_reply;
#define lorabuffer_static_init(_buf, _cap) { \
  (_cap), 0, 0, (_buf) \
}

//#pragma pack(1)

#define lorabuffer_is_empty(_lp) ((_lp)->start == (_lp)->end)

#define lorabuffer_is_full(_lp) (((_lp)->start + 1) % (_lp)->cap == (_lp)->end)

#define lorabuffer_available(_lp) (((_lp)->start + (_lp)->cap - (_lp)->end) % (_lp)->cap)

#define COMMAND_RESET 0
#define COMMAND_LED  1
#define COMMAND_VOICE 2

typedef struct {
  uint16_t cap;
  uint16_t start;
  uint16_t end;
  uint8_t * buffer;
} LoraBuffer;

typedef struct{
	int battery;
	int keyPressed;
	int led1;
	int ledLowPower;
	int charging;
	int imuCmd;
}deviceStatusStruct;

typedef enum {
  LB_OK = 0,
  LB_EMPTY,
  LB_FULL,
} LORABUFFER_RET;

typedef enum {
	LORA_AT_OK = 0,
	LORA_AT_ERR,
	LORA_AT_TIMEOUT,
	LORA_AT_TRUNCATED,
	LORA_AT_MODULE_ERROR,
	LORA_AT_CONSISTENT,
	LORA_AT_DETACH,
	LORA_AT_NO_DATA,
	LORA_AT_DATA_OVERFLOW,
} LORAAtResult;

typedef enum {
	SR_OK = 0,
	SR_EMPTY,
	SR_CONTENT,
	SR_EOF,
	SR_NO_MATCH,
} ScannerResult;

typedef struct{
	uint8_t mac[2];
	int8_t rssi;
	int8_t battery;
} HciData;

typedef struct {
  char * cmd;
  uint16_t timeout;
} LORAPredefinedCommand;

typedef struct{
	uint8_t  base_mac_nic[2];
	int8_t  rssi;
	int8_t  base_battery;
}BaseStation;

typedef struct{
//	uint8_t  offset:4;
//	uint8_t  base_num:4;
	unsigned int timestamp;
	int16_t  worker_x;
	int16_t  worker_y;
}IMUPacket_mq;

typedef struct{
	unsigned int timestamp;
	uint8_t basenum;
	HciData hcidata[5];//MAX_BEACON_DATA
}HCIPacket_mq;

typedef struct{
	uint8_t  offset:4;
	uint8_t  base_num:4;
	int16_t  worker_x;
	int16_t  worker_y;
	BaseStation  base_data[5];
}SensorData;

typedef struct
{
	uint8_t head1;
	uint8_t head2;
	uint8_t  restarted:1;
	uint8_t  fall_down:1;
	uint8_t  gps_flag:1;
	uint8_t reserved:5;
	int8_t  device_battery;
	uint32_t count;
	SensorData sensor_data[5];
#if DEVICE_HAS_GPS
	uint8_t gps_data[21];
#endif
	uint8_t  check;
	uint8_t tail1;
	uint8_t tail2;
}LoraReport;


typedef struct{ 
	uint8_t start;
	uint8_t result;
	uint8_t end;
}ReplyData;

typedef struct{
	uint8_t start;
	uint8_t commandtype;
	uint8_t value;
	uint8_t end;
}CommandData;
//#pragma pack()
extern LoraBuffer* __rbuf;
LORABUFFER_RET lorabuffer_init(LoraBuffer* lp, uint8_t* buf, uint16_t cap);  //lora初始化
LORABUFFER_RET lorabuffer_push_isr(LoraBuffer* lp, uint8_t c);

LORABUFFER_RET lorabuffer_pop(LoraBuffer* lp, uint8_t* c);

LORABUFFER_RET loraBufferCanRead(void);  //lora可读

void lorabuffer_clear(LoraBuffer *lp);    //lora清空

void  lora_dispalyRead();   //lora读取数据显示
void  lora_ntn_dispalyRead(); //lora ntn 数据操作显示
LORAAtResult lora_ntn_regsiter();
void  lora_dispalyRead1();
LORAAtResult lora_init_regsiter();
LORAAtResult lora_net_regsiter();
LORAAtResult lora_chose_net_mode();
LORAAtResult lora_chose_ntn_mode();
void  lora_uart_write_id();
void  mcu_read_id();
void lora_write_id();
void  lora_send_net_data();
void  lora_send_net_data_important();
void  uart_write_mv_acc();
void  mcu_read_mv_acc();
void mcu_get_stand_mv_acc();

LORABUFFER_RET loraBufferedRead(char *c);   //lora读
LORAAtResult loraGetReturn(char* buf, uint16_t cap, uint32_t timeout_ms);  //lora获取
LORAAtResult Lora_send_command(char *s);    //lora发送指令
LORAAtResult Lora_send_data(const void *s);  //lora发送数据
LORAAtResult lora_set_bate(void);            //设置
LORAAtResult lora_wait_response(uint16_t timeout_ms);
LORAAtResult lora_execute(char * s, uint16_t timeout_ms);   //执行
LORAAtResult lora_play_command(LORAPredefinedCommand * command);
LORAAtResult lora_play_command_set(LORAPredefinedCommand * set[], uint8_t *step);
uint8_t lora_handle_command(void);  //处理
LORAAtResult lora_regsiter(void);  //注册

void lora_off(void);  //lora关闭
#endif
