#ifndef __DELAY_H_
#define __DELAY_H_

#include "source.h"
void Delay1ms();
void delay_ms(int t);

void Delay100ms();
void delay_100ms( int t);
void Delay1000ms();
void delay_1000ms(int t);
void delay_10us(int t);
void Delay10us()	;
void delay_1us(uint32_t t)	;
#ifdef	__cplusplus
extern "C" {
#endif

#ifdef	__cplusplus
}
#endif
#endif
