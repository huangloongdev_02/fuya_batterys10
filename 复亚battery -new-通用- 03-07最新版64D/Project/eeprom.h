#ifndef __EEPROM_H_
#define __EEPROM_H_



#include "source.h"
	void IapIdle();
	char IapRead(int addr);
	void IapProgram(int addr, char dat);
	void IapErase(int addr);
		
#endif
