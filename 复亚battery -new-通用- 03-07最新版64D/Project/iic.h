#ifndef __IIC_H_
#define __IIC_H_
                 
#include "source.h"
void Wait();
void Start();
void SendData(char dat);
void RecvACK();
char RecvData();
void SendACK();
void SendNAK();
void Stop();
void Delay();
void InitMPU6050();	
//unsigned char Single_ReadI2C(unsigned char REG_Address);
uchar Single_ReadI2C(uchar REG_Address);
void Single_WriteI2C(unsigned char REG_Address,unsigned char REG_data);	


#ifdef	__cplusplus
extern "C" {
#endif



#ifdef	__cplusplus
}
#endif
#endif
