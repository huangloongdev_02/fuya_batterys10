#include "time3.h"
uint16_t  measure_time;
uint16_t  ctime ;
uint16_t  ctime_1s ;
uint16_t  send_important_ctime_1s = 0;
//int ctime ;
//int ctime_1s ;

void Timer6Init(void)		//100微秒@11.0592MHz
{
  //	AUXR |= 0x04;		//定时器时钟1T模式
//	T2L = 0xAE;		//设置定时初值
//	T2H = 0xFB;		//设置定时初值
	T4T3M |= 0x02;		//定时器时钟1T模式
	T3L = 0xAE;		//设置定时初值
	T3H = 0xFB;		//设置定时初值
	//AUXR |= 0x10;		//定时器2开始计时
	T4T3M |= 0x08;		//定时器3开始计时
	IE2 |= 0x20;     //使能定时器3中断
}

void TM3_Isr() interrupt 19
{
    ctime++; 
    measure_time++;
    
   // AUXINTIF &= ~T2IF;                          //清中断标志
	AUXINTIF &= ~T3IF;                          //清中断标志
}
