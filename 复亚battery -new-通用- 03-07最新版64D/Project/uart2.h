#ifndef __UART2_H_
#define __UART2_H_


#ifdef	__cplusplus
extern "C" {
#endif
#include "source.h"
void Uart2SendStr(char *p);
void Uart2Send(char dat);
void Uart2Init();
void Xactual_val_get(void);
 void Vmax_val_get(void);	
#ifdef	__cplusplus
}
#endif
#endif
