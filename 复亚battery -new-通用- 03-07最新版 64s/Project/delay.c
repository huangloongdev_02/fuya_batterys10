#include "delay.h"
void Delay100ms()		//@11.0592MHz
{
	unsigned char i, j, k;
	_nop_();
	i = 6;
	j = 157;
	k = 59;
	do
	{
		do
		{
			while (--k);
		} while (--j);
	} while (--i);
}

void delay_100ms(int t)
{
	int i=0;
	for(i=0;i<t;i++)
	{
	 Delay100ms();
	}
}
void delay_1000ms(int t)	
{
	int i=0;
	for(i=0;i<t;i++)
	{
	 Delay1000ms();
	}
}

void Delay1000ms()		//@11.0592MHz
{
	unsigned char i, j, k;

	_nop_();
	_nop_();
	i = 43;
	j = 6;
	k = 203;
	do
	{
		do
		{
			while (--k);
		} while (--j);
	} while (--i);
}

void Delay1ms()		//@11.0592MHz
{
	unsigned char i, j;

	i = 15;
	j = 90;
	do
	{
		while (--j);
	} while (--i);
}

void delay_ms(int t)	
{
	int i=0;
	for(i=0;i<t;i++)
	{
	 Delay1ms();
	}
}

void Delay10us()		//@11.0592MHz
{
	unsigned char i;

	_nop_();
	i = 25;
	while (--i);
}

void delay_10us(int t)	
{
	int i=0;
	for(i=0;i<t;i++)
	{
	Delay10us();		//@11.0592MHz
	}
}


void Delay1us()		//@11.0592MHz
{
	_nop_();
	_nop_();
	_nop_();
}

void delay_1us(uint32_t t)	
{
	uint32_t i=0;
	for(i=0;i<t;i++)
	{
	Delay1us();
	}
}
