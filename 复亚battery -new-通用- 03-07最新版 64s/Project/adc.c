#include "adc.h"
#include "source.h"


#define ADC_CHS_MASK (0x0f)
#define ADC_CALIBRATE_REPEATS		(8u)

const uint32_t volt2CapLot[] = {420, 397, 387, 379, 373, 368, 365, 362, 358, 351, 300};
//const uint32_t volt2CapLot[] = {350, 330, 320, 300, 270, 260, 251, 240, 230, 200, 190};
//#define ZEMCT131_RATIO_MV_TO_MA(tt)   ((tt) * 10 / 3)
//sbit    EADC        =   IE^5;

//sfr     P1M0        =   0x92;
//sfr     P1M1        =   0x91;
 uint16_t __vref = 0;
 char adc_date1;
// char adc_date2;
 float adc_num =0;
 float v_sum = 0;
 float adc_result = 0;
 float adc_result_cmpere=0;
 float   current_current = 0;
 float   Bo_voltage = 0; //飞机启动后输出电压
 uchar   Power_compare = 0;//用来过滤电量百分比的值
void calibrate_adc_vref() {
	uint8_t idx;
	uint32_t raw_sum;
	uint16_t bgv = bandgap_voltage();
//	if (bgv < 500 || bgv > 3000) {
//		log_log(LOG_ERROR, "bgv value out of range %u\r\n", bgv);
//		hardware_reboot();
//	}
// 	log_log(LOG_INFO, "BGV %u\r\n", bgv);

	for (raw_sum = 0, idx = 0; idx < ADC_CALIBRATE_REPEATS; idx++) {
		raw_sum += adc_read_channel(15);
		delay_ms(1);
	}
//    log_log(LOG_DEBUG, "internal ref sum(%u): %lu\r\n", ADC_CALIBRATE_REPEATS, raw_sum);
	__vref = (uint16_t)(0.5 + (bgv * 4095.0 / raw_sum * ADC_CALIBRATE_REPEATS));
	
// 	log_log(LOG_INFO, "calibrated Vref=%umV\r\n", __vref);
}

void ADC_read_stand() 
{
   float adc_result_temp=0;
	 calibrate_adc_vref();
//	adc_result = __vref*1.0 /1000.0* adc_num / 4096.0 ; 
//	 adc_result = 4.88*1.0 * adc_num / 4096.0 ;
		adc_result = adc_vref_one*1.0 * adc_num / 4095.0 ; 	
	
}

void ADC_read() 
{
//				adc_result = 5.0 * adc_num / 4096 ; 
   float adc_result_temp=0;
	 calibrate_adc_vref();
	adc_result = __vref*1.0 /1000.0* adc_num / 4095.0 ; 
//	 adc_result = 4.88*1.0 * adc_num / 4096.0 ; 
//	adc_result_cmpere = __vref*1.0 /1000.0* adc_num / 4096 ; 
//	adc_result_temp = adc_result_cmpere -adc_result;
//	
//	if((adc_result_temp >=0.06) ||(adc_result_temp <= -0.06)){
//	  adc_result = adc_result_cmpere;
//	
//	}
	//			adc_result = 4.88*adc_num / 4096 ; 
//		printf( "calibrated Vref=%umV\r\n", __vref);		
}


//
//void save_battery_percentage_to_power(uint8_t channel)
//{
//	    int i = 0;
//	    int adc_sum=0;
//	  
//	   // adc_num = adc_read_channel(channel);
//	    for(i=0;i<10;i++){
//				calibrate_adc_vref();
//			  adc_num = adc_read_channel(channel);
//				adc_sum += adc_num;
//			}
//			adc_num = adc_sum /10;
//			ADC_read();
////			printf("%d adc_result = %f\r\n",(int)channel,adc_result);	
////		  battery_percentage = getBatteryCap(adc_result);
////	    power = battery_percentage;
////	    printf("power = %d\r\n",(int)power);
//	
//}
//获取电压值
void get_channel_power(uint8_t channel){
  int i = 0;
	    int32_t adc_sum=0;
	    int adc_temp[10]={0};
			int adc_t1;
	   ADC_CONTR = 0x80 | channel;                           //使能ADC模块  设置ADC口
	   // adc_num = adc_read_channel(channel);
	     calibrate_adc_vref();
			  for(i=0;i<10;i++){
			  adc_temp[i] = adc_read_channel(channel);		
			}
			 for(i=1;i<10;i++){  //找出最小的
			  if(adc_temp[0] > adc_temp[i]){
					adc_t1 = adc_temp[i];
					adc_temp[i] = adc_temp[0];
					adc_temp[0] = adc_t1;;
				}
			}
			 for(i=2;i<10;i++){  //找出第二小的
			  if(adc_temp[1] > adc_temp[i]){
					adc_t1 = adc_temp[i];
					adc_temp[i] = adc_temp[1];
					adc_temp[1] = adc_t1;
				}
			}
				for(i=3;i<10;i++){  //找出最大的
			  if(adc_temp[2] < adc_temp[i]){
					adc_t1 = adc_temp[i];
					adc_temp[i] = adc_temp[2];
					adc_temp[2] = adc_t1;;
				}
			}
					for(i=4;i<10;i++){  //找出第二大的
			  if(adc_temp[3] < adc_temp[i]){
					adc_t1 = adc_temp[i];
					adc_temp[i] = adc_temp[3];
					adc_temp[3] = adc_t1;;
				}
			}
				
			 for(i=4;i<10;i++){
			  adc_sum +=  adc_temp[i];
			//	adc_sum += adc_num;
			}
			adc_num = adc_sum /6.0;
			ADC_read();
//			if(channel == 5){
//				printf("%d adc_result = %f\r\n",(int)channel,adc_num);	
//			}
}
//电压转化 关机不充电 1
void Convert_external_voltage(void){
	   //电量百分比
		  float k1,k2,k3,k4,k5;
	   float temp_pow=0;
	   int32_t compare_value=0;
	   k1 = (99-83)/(17.48-16.83);   
	   k2 = (83-65)/(16.83-16.14);
	   k3 = (65-27)/(16.14-15.22);
	   k4 = (27-13)/(15.22-14.81);  //
       k5 = (13)/(14.81-14.40);  //
	   temp_pow = adc_result *5.7 + 0.05;    //					
	//	printf("temp_pow = %f\r\n",temp_pow);
	//  if(temp_pow>17.35 || temp_pow <0){
	//	  temp_pow = 100.0;
	//	}else 
    if(16.83<temp_pow){  //第一阶段	
		  temp_pow =  k1 *(temp_pow-16.83)+83;
		}else if( 16.14<temp_pow){  //第二阶段
		  temp_pow =  k2 *(temp_pow-16.14)+65;
		}else if( 15.22<temp_pow){  //第三阶段
		   temp_pow =  k3 *(temp_pow-15.22)+27;
		}else if( 14.81<temp_pow){  //第四阶段
		   temp_pow =  k4 *(temp_pow-14.81)+13;
		}else if( 14.40<temp_pow){  //第五阶段
		   temp_pow =  k5 *(temp_pow-14.40);
		}else {
		    temp_pow = 0.0;
		}
			
			Power_compare = (int)temp_pow;
		  
			 compare_value = current_power-Power_compare; //获取电量值的差值  98 100
//		 if(P34 == 1){
			 if(Power_compare >= 102){
				 current_power = 100;
			 }else if((compare_value==-3)){        
				   current_power = current_power+1;
				}else if(compare_value==3){
				   current_power = current_power-1;
				}else if((compare_value<3) && (compare_value>-3)){
					//	current_power =current_power;  //电量值保持不变  
					
				}else if(compare_value > 3 || compare_value < -3){		 
						current_power = (int)temp_pow;
				}
				//当百分比大于100的时候
				 if(current_power >= 100){
				 current_power = 100;	
				 }					 
				//	current_power = (int)temp_pow;				
//		 }
//		  if(P34 == 0){
//				if(((current_power-Power_compare)>=2)||((current_power-Power_compare)<=-2)||(Power_compare == 100)){
//				   current_power = Power_compare;
////					printf(" power_compare start\r\n");
//				}
//				
//			}
		if(PRINT_CHOSE){
				printf("关机不充电\r\n");
				printf("current_power = %d\r\n",(int)current_power);
		}
		// adc_result = adc_result* 5.7+0.05;
		 adc_result = adc_result* 5.7;
		if(PRINT_CHOSE){
		   printf("adc_result + 0.05 = %f\r\n",adc_result);
		}
			current_voltage = (int) ((adc_result)*10);												//当前电压 乘上10倍
		if(PRINT_CHOSE){
	    printf("current_voltage = %d\r\n",current_voltage);
		}			
} 
//关机充电 2
void Power_off_charging_voltage(void){
	 	   //电量百分比
	 
       float k1,k2,k3,k4,k5;
	   float temp_pow=0;
	   int32_t compare_value=0;
	   k1 = (100-83)/(17.88-17.30);   
	   k2 = (83-69)/(17.30-16.82);
	   k3 = (69-40)/(16.82-15.79);
	   k4 = (40-13)/(15.79-15.22);  //
       k5 = (13)/(15.22-14.80);  //
	   temp_pow = adc_result *5.7 + 0.05;    //					
	//	printf("temp_pow = %f\r\n",temp_pow);
	//  if(temp_pow>17.35 || temp_pow <0){
	//	  temp_pow = 100.0;
	//	}else 
    if(17.30<temp_pow){  //第一阶段	
		  temp_pow =  k1 *(temp_pow-17.30)+83;
		}else if( 16.82<temp_pow){  //第二阶段
		  temp_pow =  k2 *(temp_pow-16.82)+69;
		}else if( 15.79<temp_pow){  //第三阶段
		   temp_pow =  k3 *(temp_pow-15.79)+40;
		}else if( 15.22<temp_pow){  //第四阶段
		   temp_pow =  k4 *(temp_pow-15.22)+13;
		}else if( 14.80<temp_pow){  //第五阶段
		   temp_pow =  k5 *(temp_pow-14.80);
		}else {
		    temp_pow = 0.0;
		}
			Power_compare = (int)temp_pow;
		  
			 compare_value = current_power-Power_compare; //获取电量值的差值  98 100
			 if(Power_compare >= 102){
				 current_power = 100;
			 }else if((compare_value==-3)){        
				   current_power = current_power+1;
				}else if(compare_value==3){
				   current_power = current_power-1;
				}else if((compare_value<3) && (compare_value>-3)){
					//	current_power =current_power;  //电量值保持不变  
					
				}else if(compare_value > 3 || compare_value < -3){		 
						current_power = (int)temp_pow;
				}
				//当百分比大于100的时候
				 if(current_power >= 100){
				 current_power = 100;	
				 }
				
		if(PRINT_CHOSE){
			printf("关机充电\r\n");
		  printf("current_power = %d\r\n",(int)current_power);
		}
		  adc_result = adc_result* 5.7+0.05;
		if(PRINT_CHOSE){
		  printf("adc_result + 0.05 = %f\r\n",adc_result);
		}
			current_voltage = (int) ((adc_result)*10);												//当前电压 乘上10倍
		if(PRINT_CHOSE){
	  //  printf("current_voltage = %d\r\n",current_voltage);
		}			
}

//开机充电 3
void Power_on_charging_voltage(void){
	   //电量百分比
	   float k1,k2,k3,k4;
	   float temp_pow=0;
	   int32_t compare_value=0;
	   k1 = (100-66)/(17.76-16.37);   
	   k2 = (66-27)/(16.37-15.44);
	   k3 = (27-10)/(15.44-14.97);
	   k4 = (10)/(14.97-14.40);  //
	   temp_pow = adc_result *5.7 + 0.05 ;    //					
	//	printf("temp_pow = %f\r\n",temp_pow);
	//  if(temp_pow>17.85 || temp_pow <0){
	//	  temp_pow = 100.0;
	//	}else
    if( 16.37<temp_pow){  //第一阶段	
		  temp_pow =  k1 *(temp_pow-16.37)+66;
		}else if( 15.44<temp_pow){  //第二阶段
		  temp_pow =  k2 *(temp_pow-15.44)+27;
		}else if( 14.97<temp_pow){  //第三阶段
		   temp_pow =  k3 *(temp_pow-14.97)+10;
		}else if( 14.40<temp_pow){  //第四阶段
		   temp_pow =  k4 *(temp_pow-14.40);
		}else {
		    temp_pow = 0.0;
		}

			Power_compare = (int)temp_pow;
		  
			 compare_value = current_power-Power_compare; //获取电量值的差值  98 100
			 if(Power_compare >= 102){
				 current_power = 100;
			 }else if((compare_value==-3)){        
				   current_power = current_power+1;
				}else if(compare_value==3){
				   current_power = current_power-1;
				}else if((compare_value<3) && (compare_value>-3)){
					//	current_power =current_power;  //电量值保持不变  
					
				}else if(compare_value > 3 || compare_value < -3){		 
						current_power = (int)temp_pow;
				}
				//当百分比大于100的时候
				 if(current_power >= 100){
				 current_power = 100;	
				 }
				
		if(PRINT_CHOSE){
			printf("开机充电\r\n");
		  printf("current_power = %d\r\n",(int)current_power);
		}
		 adc_result = adc_result* 5.7+0.05;
		if(PRINT_CHOSE){
		  printf("adc_result + 0.05 = %f\r\n",adc_result);
		}
			current_voltage = (int) ((adc_result)*10);												//当前电压 乘上10倍
		if(PRINT_CHOSE){
	  //  printf("current_voltage = %d\r\n",current_voltage);
		}			
}
//开机不充电 4 
void Power_on_nocharging_voltage(void){
	     //电量百分比
		 float k1,k2,k3,k4,k5;
		 float temp_pow=0;
	   int32_t compare_value=0;
	   k1 = (99-89)/(17.35-16.92);   
	   k2 = (89-68)/(16.92-16.04);
	   k3 = (68-34)/(16.04-15.10);
	   k4 = (34-10)/(15.10-14.67);  //
     k5 = (10)/(14.67-14.40);  //
	   temp_pow = adc_result *5.7 + 0.05;    //					
	//	printf("temp_pow = %f\r\n",temp_pow);
	//  if(temp_pow>17.35 || temp_pow <0){
	//	  temp_pow = 100.0;
		//}else
		if( 16.92<temp_pow){  //第一阶段	
		  temp_pow =  k1 *(temp_pow-16.92)+89;
		}else if( 16.04<temp_pow){  //第二阶段
		  temp_pow =  k2 *(temp_pow-16.04)+68;
		}else if( 15.10<temp_pow){  //第三阶段
		   temp_pow =  k3 *(temp_pow-15.10)+34;
		}else if( 14.67<temp_pow){  //第四阶段
		   temp_pow =  k4 *(temp_pow-14.67)+10;
		}else if( 14.40<temp_pow){  //第五阶段
		   temp_pow =  k5 *(temp_pow-14.40);
		}else {
		    temp_pow = 0.0;
		}
			Power_compare = (int)temp_pow;
		  
			 compare_value = current_power-Power_compare; //获取电量值的差值  98 100
//		 if(P34 == 1){
				 if(Power_compare >= 102){
				 current_power = 100;
			 }else if((compare_value==-3)){        
				   current_power = current_power+1;
				}else if(compare_value==3){
				   current_power = current_power-1;
				}else if((compare_value<3) && (compare_value>-3)){
					//	current_power =current_power;  //电量值保持不变  
			
				}else if(compare_value > 3 || compare_value < -3){		 
						current_power = (int)temp_pow;
				}
				//当百分比大于100的时候
				 if(current_power >= 100){
				 current_power = 100;	
				 }
//		 }
//		  if(P34 == 0){
//				if(((current_power-Power_compare)>=2)||((current_power-Power_compare)<=-2)||(Power_compare == 100)){
//				   current_power = Power_compare;
////					printf(" power_compare start\r\n");
//				}
//				
//			}
		if(PRINT_CHOSE){
			printf("开机不充电\r\n");
		  printf("current_power = %d\r\n",(int)current_power);
		}
	//	 adc_result = adc_result* 5.7 + 0.05;
		adc_result = adc_result* 5.7;  
		if(PRINT_CHOSE){
		  printf("adc_result + 0.05 = %f\r\n",adc_result);
		}
			current_voltage = (int) ((adc_result)*10);												//当前电压 乘上10倍
		if(PRINT_CHOSE){
	  //  printf("current_voltage = %d\r\n",current_voltage);
		}			
}

//电流转化
void Convert_external_current(void){
  adc_result = adc_result*2;
	current_current = adc_result;
	if(PRINT_CHOSE){
 // printf("current_current = %f\r\n",current_current);
	}
}


//飞机开机电压检测
void Boot_voltage (void){
	  Bo_voltage = adc_result* 5.7;
	if(PRINT_CHOSE ){
		//printf("Bo_voltage *5.7 = %f\r\n",Bo_voltage);
    //飞机启动后输出电压
	}

}

//获取电池电量百分比
int getBatteryCap(float volt)
{
	int a1, a2, b1, b2;
	uint16_t x;
	float A, B, y;
	int out, i;
	 x =(uint16_t)(volt * 100);
//	rt_kprintf("the BatteryCap is :%d \n",x);
//printf("volat2caplot sizeof(volt2CapLot) = %d\r\n",(int)(sizeof(volt2CapLot)/sizeof(volt2CapLot[0])));
//	for(j=0;j<sizeof(volt2CapLot);j++)
//	{
//	  printf("j = %d ",j);
//	}
	if (x >= volt2CapLot[0])
	{
		return 100;
	}
	if (x < volt2CapLot[9])
	{
		return 0;
	}
	
	for (i = 0; i < sizeof(volt2CapLot)/sizeof(volt2CapLot[0]); i++)
	{
		if (abs(x - volt2CapLot[i]) <= 3)
		{
			return (10 - i) * 10;
		}
		else if (x > volt2CapLot[i + 1] && x < volt2CapLot[i])
		{
			b1 = 10 * (10 - i);
			b2 = 10 * (9 - i);
			a1 = volt2CapLot[i];
			a2 = volt2CapLot[i + 1];
			A = (b1 - b2) / (a1 - a2); // 电压百分比的差值   /  测得数据的区间
			B = (a2 * b1 - a1 * b2) / (a2 - a1);  //
			y = A * x + B;
			out = (int)y;
			if(out >= b1)out = b1;
			if(out <= b2)out = b2;
			//printf("i:%d,,a1:%f,,a2:%f,,b1:%f,,b2:%f,,A:%f,,B:%f,,y:%f,,x:%f,,out:%f,\n",i,a1,a2,b1,b2,A,B,y,x,out);
			
			return out;
		}
	}
	return out;
}
 
//等待转换
static void _wait_conversion() 
{
	if (!(ADC_CONTR & ADC_START)) {
		return;
	}
	while(!(ADC_CONTR & ADC_FLAG));
}

//adc读取通道
uint16_t adc_read_channel(uint8_t channel)
{
	uint16_t val = 0;
	channel &= ADC_CHS_MASK;
	// wait in case last conversion still running
	_wait_conversion();
	ADC_CONTR &= ~ADC_FLAG;
	ADC_CONTR = ADC_START | (ADC_CONTR & ~ADC_CHS_MASK) | channel;
	_nop_();
	_nop_();
	_wait_conversion();
	val = 0x0fff & ((((uint16_t)ADC_RES) << 8) | ((uint16_t)ADC_RESL));
	// log_log(LOG_DEBUG, "adc ch %bu = %u\r\n", channel, val);
//	printf("val = %u\r\n",val);
	return val;
}

uint16_t bandgap_voltage() {
	// read from RAM
	uint16_t *bgv = (uint16_t idata*)0xef;
	// read from ROM (must burn with proper option)
	// uint16_t *bgv = (uint16_t code*)0x7ff7;
	return *bgv;
} 




//获取电压值
void get_channel_power_5(uint8_t channel){
      int i = 0;
	    int32_t adc_sum=0;
	    int adc_temp[10]={0};
			int adc_t1;
	   ADC_CONTR = 0x80 | channel;                           //使能ADC模块  设置ADC口
	   // adc_num = adc_read_channel(channel);
	     calibrate_adc_vref();
			  for(i=0;i<10;i++){
			  adc_temp[i] = adc_read_channel(channel);		
			}
			 for(i=1;i<10;i++){  //找出最小的
			  if(adc_temp[0] > adc_temp[i]){
					adc_t1 = adc_temp[i];
					adc_temp[i] = adc_temp[0];
					adc_temp[0] = adc_t1;;
				}
			}
			 for(i=2;i<10;i++){  //找出第二小的
			  if(adc_temp[1] > adc_temp[i]){
					adc_t1 = adc_temp[i];
					adc_temp[i] = adc_temp[1];
					adc_temp[1] = adc_t1;
				}
			}
				for(i=3;i<10;i++){  //找出最大的
			  if(adc_temp[2] < adc_temp[i]){
					adc_t1 = adc_temp[i];
					adc_temp[i] = adc_temp[2];
					adc_temp[2] = adc_t1;;
				}
			}
					for(i=4;i<10;i++){  //找出第二大的
			  if(adc_temp[3] < adc_temp[i]){
					adc_t1 = adc_temp[i];
					adc_temp[i] = adc_temp[3];
					adc_temp[3] = adc_t1;;
				}
			}
				
			 for(i=4;i<10;i++){
			  adc_sum +=  adc_temp[i];
			//	adc_sum += adc_num;
			}
			adc_num = adc_sum /6.0;
			ADC_read_stand();
			if(channel == 5){
			//	printf("%d adc_result = %f\r\n",(int)channel,adc_num);	
			}
}


//


//400ms关闭测试电压
void Close_power_get_5(void){
  					int i;
								//飞机关机且未充电模式，adc 动态获取数据
								P27 = 0;//开始检测
								delay_100ms(4);
									for(i=0;i<5;i++){
										get_channel_power_5(5);   //adc强制标定
									//	get_channel_power(5);   //adc动态标定
										v_sum += adc_num;
									}
									adc_num = v_sum/5.0;
			    // adc_num =5.0;
								if(P34 ==1){
								P27 = 1;//结束检测	
								}
}




//标定开机不充电 4 
void Power_on_nocharging_voltage_stand(float adc){
	     //电量百分比
		 float k1,k2,k3,k4,k5;
		 float temp_pow=0;
	   int32_t compare_value=0;
	   k1 = (99-89)/(17.35-16.92);   
	   k2 = (89-68)/(16.92-16.04);
	   k3 = (68-34)/(16.04-15.10);
	   k4 = (34-10)/(15.10-14.67);  //
     k5 = (10)/(14.67-14.40);  //
//	   temp_pow = adc_result *5.7;    //	
      temp_pow = adc;	
	//	printf("temp_pow = %f\r\n",temp_pow);
	//  if(temp_pow>17.35 || temp_pow <0){
	//	  temp_pow = 100.0;
		//}else
		if( 16.92<temp_pow){  //第一阶段	
		  temp_pow =  k1 *(temp_pow-16.92)+89;
		}else if( 16.04<temp_pow){  //第二阶段
		  temp_pow =  k2 *(temp_pow-16.04)+68;
		}else if( 15.10<temp_pow){  //第三阶段
		   temp_pow =  k3 *(temp_pow-15.10)+34;
		}else if( 14.67<temp_pow){  //第四阶段
		   temp_pow =  k4 *(temp_pow-14.67)+10;
		}else if( 14.40<temp_pow){  //第五阶段
		   temp_pow =  k5 *(temp_pow-14.40);
		}else {
		    temp_pow = 0.0;
		}
			Power_compare = (int)temp_pow;
		   printf("temp_pow = %d\r\n",(int)temp_pow);
			 compare_value = current_power-Power_compare; //获取电量值的差值  98 100
//		 if(P34 == 1){
		
//				 if(Power_compare >= 97){
//				 current_power = 100;
//			 }
				if(Power_compare <= 90){
				 if((compare_value==-3)){        
				   current_power = current_power+1;
				}else if(compare_value==3){
				   current_power = current_power-1;
				}else if((compare_value<3) && (compare_value>-3)){
					//	current_power =current_power;  //电量值保持不变  
			
				}else if(compare_value > 3 || compare_value < -3){		 
						current_power = (int)temp_pow;
				}
			}else if(Power_compare > 90 ){
				 if((compare_value==-2)){        
				   current_power = current_power+1;
				}else if(compare_value==2){
				   current_power = current_power-1;
				}else if((compare_value<2) && (compare_value>-2)){
					//	current_power =current_power;  //电量值保持不变  			
				}else if(compare_value > 2 || compare_value < -2){		 
						current_power = (int)temp_pow;
				}
			}
				//当百分比大于100的时候
				 if(current_power >= 97){
				 current_power = 100;	
				 }
//		 }
//		  if(P34 == 0){
//				if(((current_power-Power_compare)>=2)||((current_power-Power_compare)<=-2)||(Power_compare == 100)){
//				   current_power = Power_compare;
////					printf(" power_compare start\r\n");
//				}
//				
//			}
		if(PRINT_CHOSE){
		//	printf("开机不充电\r\n");
		  printf("current_power = %d\r\n",(int)current_power);
		}
	//	 adc_result = adc_result* 5.7 + 0.05;
	//	adc_result = adc_result* 5.7;  
		if(PRINT_CHOSE){
		//  printf("adc_result + 0.05 = %f\r\n",adc_result);
		//	 printf("Battery adc_result + 0.05 = %f\r\n",adc);
		}
			current_voltage = (int) ((adc)*10);												//当前电压 乘上10倍
		if(PRINT_CHOSE){
	  //  printf("current_voltage = %d\r\n",current_voltage);
		}

}
