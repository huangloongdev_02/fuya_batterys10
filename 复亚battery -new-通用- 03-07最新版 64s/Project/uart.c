#include "uart.h"

bit busy;
char wptr;
char rptr;
uchar buffer[16];

void UartSend(char dat)
{
    while (busy);
    busy = 1;
    SBUF = dat;
}
char putchar(char ch)
{
   TI = 0;
  SBUF = ch;
  while (TI == 0);
  TI = 0;
  return 0;
}

void UartSendStr(char *p)
{
    while (*p)
    {
        UartSend(*p++);
    }
}

void UartIsr() interrupt 4
{
	if (TI)    //串口1发送中断请求标志位  模式0 8位数据传输结束后置1
    {
        TI = 0;  //必须手动清零
        busy = 0;
    }
    if (RI)  //串口1接收中断标志位  模式0  8位数据接收结束置1
    {
      RI = 0;   //必须手动清零
			buffer[wptr++] = SBUF; //串口1数据接收、发送缓冲区  
		  wptr &= 0x0f;  //保证0-15字节接收
    }	
}

void UartInit()
{
    SCON = 0x50;   // 串口1控制寄存器    可变波特率8位数据方式 允许串口接收
    TMOD = 0x00;    //定时器0/1模式寄存器
    TL1 = BRT;
    TH1 = BRT >> 8;  //定时器1计数寄存器
    TR1 = 1;				 //定时器T1的运行控制位
    AUXR = 0x40;     //定时器1不分频
    wptr = 0x00;     //写入的数据
    rptr = 0x00;     //读取到的数据
    busy = 0;
}

//串口1写入Id到flash中
  void uart1_send_id_flash(){
	//	printf("test\r\n");
	//	printf("uart1 Receives  data  %s\r\n",buffer);
	//  printf("lora_read:%s",buffer);
//  	printf("\r\n");
//	  putchar(buffer[0]);
//	  printf("\r\n");
//	 putchar(buffer[1]);
//	  printf("\r\n");
//	  putchar(buffer[2]);
	if(buffer[0] ==0x53 && buffer[6]==0x72)
	{	
		lora_uart_write_id();
		printf("uart write id success!\r\n");
		 delay_ms(1000);
	
	}else if(buffer[0] ==0x56 && buffer[5]==0x61){
	  uart_write_mv_acc();
		printf("uart write mv acc success!\r\n");
		delay_ms(1000);
	}else
	{	
		mcu_read_id(); 
		mcu_read_mv_acc();
//	  printf("uart write id error!\r\n");
//		printf("uart write mv acc error!\r\n");
	}
	  wptr = 0;	        
	  memset(buffer,0,sizeof(buffer));
}










