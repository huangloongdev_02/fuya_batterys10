#include "tmc262.h"

/////* tmc262 register */
//#define		REG_DRVCTRL			0x00000000   //驱动控制寄存器
//#define		REG_CHOPCONF		0x00080000   //斩波设置寄存器
//#define		REG_SMARTEN			0x000A0000   //步进优化设置寄存器
//#define		REG_SGCSCONF		0x000C0000   //保护和诊断设置寄存器
//#define		REG_DRVCONF			0x000E0000   //驱动器设置寄存器

/////* register value */
//#define		MICROSTEP_256		0x00
//#define		MICROSTEP_128		0x01
//#define		MICROSTEP_64		0x02
//#define		MICROSTEP_32		0x03
//#define		MICROSTEP_16		0x04
//#define		MICROSTEP_8			0x05
//#define		MICROSTEP_4			0x06
//#define		MICROSTEP_2			0x07
//#define		MICROSTEP_1			0x08

/* 默认配置 */
#define		SCG_DEFAULT			0x10000
bit busy_spi1;

void SPI_Isr() interrupt 9
{
		uchar temp=0;
   // SPSTAT = 0x40;  
    SPSTAT = 0xC0;	//清中断标志
    SS = 0;  	//拉高从机的SS管脚
//	P35=1;
	  
	
    busy_spi1 = 0;
//     printf("t");
   // LED = !LED;                                 //测试端口
 //  Send_complete_flag_spi = 1;
	
	if(spi_del_flag ==1){
		Spi_receive_buf[spi_getptr++] = SPDAT;
    spi_getptr &= 0xf;
	}
//	}
}



////驱动器配置


//总共发送五个字节 第一个是地址  后面四个是指令 
//注意写数据地址需要加上0x80
void TMC_SPIWriteInt(uint8_t reg, int32_t spi_send_data)
{
		//中断方式  
	     while (busy_spi1){
				}
        busy_spi1 = 1;
        SS = 0;                             //拉低从机SS管脚
				SPDAT = ((reg)|0x80) & 0xff;  
				
        while (busy_spi1){
				}
        busy_spi1 = 1;
       SS = 0; 
       SPDAT = (uint8_t)((spi_send_data>>24) & 0xff);
        while (busy_spi1){
				}
       busy_spi1 = 1;
        SS = 0;                             //拉低从机SS管脚
				SPDAT = (uint8_t)((spi_send_data>>16) & 0xff);  
        while (busy_spi1){
				}
        busy_spi1 = 1;
        SS = 0; 
       SPDAT = (uint8_t)((spi_send_data>>8) & 0xff);
        while (busy_spi1){
				}
       busy_spi1 = 1;
       SS = 0; 
		   SPDAT = (uint8_t)(spi_send_data & 0xff);
				while (busy_spi1){
				}		
			SS=1;
			spi_send_data	=0x000000;			
		//	delay_1000ms(1);
				delay_ms(100);
}

//总共发送五个字节 第一个是地址  后面四个是指令 
//注意写数据地址不需要需要加上0x80
void TMC_SPIReadInt(uint8_t reg, int32_t spi_send_data)
{
		//中断方式  
	     while (busy_spi1){
				}
        busy_spi1 = 1;
        SS = 0;                             //拉低从机SS管脚
				SPDAT = (reg) & 0xff;  
				
        while (busy_spi1){
				}
        busy_spi1 = 1;
       SS = 0; 
       SPDAT = (uint8_t)((spi_send_data>>24) & 0xff);
        while (busy_spi1){
				}
       busy_spi1 = 1;
        SS = 0;                             //拉低从机SS管脚
				SPDAT = (uint8_t)((spi_send_data>>16) & 0xff);  
        while (busy_spi1){
				}
        busy_spi1 = 1;
        SS = 0; 
       SPDAT = (uint8_t)((spi_send_data>>8) & 0xff);
        while (busy_spi1){
				}
       busy_spi1 = 1;
       SS = 0; 
		   SPDAT = (uint8_t)(spi_send_data & 0xff);
				while (busy_spi1){
				}		
			SS=1;
			spi_send_data	=0x000000;			
	//		delay_1000ms(1);
}

   
//void TMC_clockwise(void)

void TMC_Init(void){
//====================================================================================================//
// ACTUAL SETTINGS FOR TMC5160 (created: 2021/10/15 10:30:30)                                        //
//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv//

//	TMC_SPIWriteInt(0x00, 	0x00000008); 		// writing value 0x00000008 = 8 = 0.0 to address 0 = 0x00(GCONF)
////	TMC_SPIWriteInt(0x03, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 1 = 0x03(SLAVECONF)
////	TMC_SPIWriteInt(0x05, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 2 = 0x05(X_COMPARE)
////	TMC_SPIWriteInt(0x06, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 3 = 0x06(OTP_PROG)
//	TMC_SPIWriteInt(0x08, 	0x0000000F); 		// writing value 0x0000000F = 15 = 0.0 to address 4 = 0x08(FACTORY_CONF)
//	TMC_SPIWriteInt(0x09, 	0x00010606); 		// writing value 0x00010606 = 67078 = 0.0 to address 5 = 0x09(SHORT_CONF)
//	TMC_SPIWriteInt(0x0A, 	0x00080400); 		// writing value 0x00080400 = 525312 = 0.0 to address 6 = 0x0A(DRV_CONF)
////	TMC_SPIWriteInt(0x0B, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 7 = 0x0B(GLOBAL_SCALER)
//	TMC_SPIWriteInt(0x10, 	0x00071003); 		// writing value 0x00070203 = 459267 = 0.0 to address 8 = 0x10(IHOLD_IRUN)
//	TMC_SPIWriteInt(0x11, 	0x0000000A); 		// writing value 0x0000000A = 10 = 0.0 to address 9 = 0x11(TPOWERDOWN)
////	TMC_SPIWriteInt(0x13, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 10 = 0x13(TPWMTHRS)
////	TMC_SPIWriteInt(0x14, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 11 = 0x14(TCOOLTHRS)
////	TMC_SPIWriteInt(0x15, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 12 = 0x15(THIGH)
//	TMC_SPIWriteInt(0x20, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 13 = 0x20(RAMPMODE)
//	TMC_SPIWriteInt(0x21, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 14 = 0x21(XACTUAL)
////	TMC_SPIWriteInt(0x23, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 15 = 0x23(VSTART)
//	TMC_SPIWriteInt(0x24, 	0x000003E8); 		// writing value 0x000003E8 = 1000 = 0.0 to address 16 = 0x24(A1)
//	TMC_SPIWriteInt(0x25, 	0x0000c350); 		// writing value 0x00000000 = 0 = 0.0 to address 17 = 0x25(V1)
//	TMC_SPIWriteInt(0x26, 	0x00001388); 		// writing value 0x000001F4 = 500 = 0.0 to address 18 = 0x26(AMAX)
//	TMC_SPIWriteInt(0x27, 	0x000186A0); 		// writing value 0x00002710 = 10000 = 0.0 to address 19 = 0x27(VMAX)
//	TMC_SPIWriteInt(0x28, 	5000); 		// writing value 0x000001F4 = 500 = 0.0 to address 20 = 0x28(DMAX)
//	TMC_SPIWriteInt(0x2A, 	0x00000578); 		// writing value 0x00000578 = 1400 = 0.0 to address 21 = 0x2A(D1)
//	TMC_SPIWriteInt(0x2B, 	0x0000000A); 		// writing value 0x0000000A = 10 = 0.0 to address 22 = 0x2B(VSTOP)
////	TMC_SPIWriteInt(0x2C, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 23 = 0x2C(TZEROWAIT)
//	TMC_SPIWriteInt(0x2D, 	0x00); 		// writing value 0x00000000 = 0 = 0.0 to address 24 = 0x2D(XTARGET)
//	TMC_SPIWriteInt(0x33, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 25 = 0x33(VDCMIN)
//	TMC_SPIWriteInt(0x34, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 26 = 0x34(SW_MODE)
//	TMC_SPIWriteInt(0x38, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 27 = 0x38(ENCMODE)
//	TMC_SPIWriteInt(0x39, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 28 = 0x39(X_ENC)
//	TMC_SPIWriteInt(0x3A, 	0x00010000); 		// writing value 0x00010000 = 65536 = 0.0 to address 29 = 0x3A(ENC_CONST)
//  TMC_SPIWriteInt(0x3D, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 30 = 0x3D(ENC_DEVIATION)
//	TMC_SPIWriteInt(0x60, 	0xAAAAB554); 		// writing value 0xAAAAB554 = 0 = 0.0 to address 31 = 0x60(MSLUT[0])
//	TMC_SPIWriteInt(0x61, 	0x4A9554AA); 		// writing value 0x4A9554AA = 1251300522 = 0.0 to address 32 = 0x61(MSLUT[1])
//	TMC_SPIWriteInt(0x62, 	0x24492929); 		// writing value 0x24492929 = 608774441 = 0.0 to address 33 = 0x62(MSLUT[2])
//	TMC_SPIWriteInt(0x63, 	0x10104222); 		// writing value 0x10104222 = 269500962 = 0.0 to address 34 = 0x63(MSLUT[3])
//	TMC_SPIWriteInt(0x64, 	0xFBFFFFFF); 		// writing value 0xFBFFFFFF = 0 = 0.0 to address 35 = 0x64(MSLUT[4])
//	TMC_SPIWriteInt(0x65, 	0xB5BB777D); 		// writing value 0xB5BB777D = 0 = 0.0 to address 36 = 0x65(MSLUT[5])
//	TMC_SPIWriteInt(0x66, 	0x49295556); 		// writing value 0x49295556 = 1227445590 = 0.0 to address 37 = 0x66(MSLUT[6])
//	TMC_SPIWriteInt(0x67, 	0x00404222); 		// writing value 0x00404222 = 4211234 = 0.0 to address 38 = 0x67(MSLUT[7])
//	TMC_SPIWriteInt(0x68, 	0xFFFF8056); 		// writing value 0xFFFF8056 = 0 = 0.0 to address 39 = 0x68(MSLUTSEL)
//	TMC_SPIWriteInt(0x69, 	0x00F70000); 		// writing value 0x00F70000 = 16187392 = 0.0 to address 40 = 0x69(MSLUTSTART)
//	TMC_SPIWriteInt(0x6C, 	0x00410153); 		// writing value 0x00410153 = 4260179 = 0.0 to address 41 = 0x6C(CHOPCONF)
//	
//	TMC_SPIWriteInt(0x6D, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 42 = 0x6D(COOLCONF)
//	TMC_SPIWriteInt(0x6E, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 43 = 0x6E(DCCTRL)
//	TMC_SPIWriteInt(0x70, 	0xC40C001E); 		// writing value 0xC40C001E = 0 = 0.0 to address 44 = 0x70(PWMCONF)

//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^//
//====================================================================================================//
// ACTUAL SETTINGS FOR TMC5160 (created: 2021/10/26 10:35:11)                                        //
//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv//
	TMC_SPIWriteInt(0x00, 	0x00000008); 		// writing value 0x00000008 = 8 = 0.0 to address 0 = 0x00(GCONF)
	TMC_SPIWriteInt(0x03, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 1 = 0x03(SLAVECONF)
	TMC_SPIWriteInt(0x05, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 2 = 0x05(X_COMPARE)
	TMC_SPIWriteInt(0x06, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 3 = 0x06(OTP_PROG)
	TMC_SPIWriteInt(0x08, 	0x0000000D); 		// writing value 0x0000000D = 13 = 0.0 to address 4 = 0x08(FACTORY_CONF)
	TMC_SPIWriteInt(0x09, 	0x00010606); 		// writing value 0x00010606 = 67078 = 0.0 to address 5 = 0x09(SHORT_CONF)
	TMC_SPIWriteInt(0x0A, 	0x00080400); 		// writing value 0x00080400 = 525312 = 0.0 to address 6 = 0x0A(DRV_CONF)
	TMC_SPIWriteInt(0x0B, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 7 = 0x0B(GLOBAL_SCALER)
	TMC_SPIWriteInt(0x10, 	0x00071603); 		// writing value 0x00070A03 = 461315 = 0.0 to address 8 = 0x10(IHOLD_IRUN)
	TMC_SPIWriteInt(0x11, 	0x0000000A); 		// writing value 0x0000000A = 10 = 0.0 to address 9 = 0x11(TPOWERDOWN)
	TMC_SPIWriteInt(0x13, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 10 = 0x13(TPWMTHRS)
	TMC_SPIWriteInt(0x14, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 11 = 0x14(TCOOLTHRS)
	TMC_SPIWriteInt(0x15, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 12 = 0x15(THIGH)
	TMC_SPIWriteInt(0x20, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 13 = 0x20(RAMPMODE)
	TMC_SPIWriteInt(0x21, 	0x00000000); 		// writing value 0x00000001 = 1 = 0.0 to address 14 = 0x21(XACTUAL)
	TMC_SPIWriteInt(0x23, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 15 = 0x23(VSTART)
	TMC_SPIWriteInt(0x24, 	0x00001388); 		// writing value 0x00001388 = 5000 = 0.0 to address 16 = 0x24(A1)
	TMC_SPIWriteInt(0x25, 	0x00001388); 		// writing value 0x00001388 = 5000 = 0.0 to address 17 = 0x25(V1)
	TMC_SPIWriteInt(0x26, 	0x00001388); 		// writing value 0x00001388 = 5000 = 0.0 to address 18 = 0x26(AMAX)
	TMC_SPIWriteInt(0x27, 	0x000186A0); 		// writing value 0x000186A0 = 100000 = 0.0 to address 19 = 0x27(VMAX)
	TMC_SPIWriteInt(0x28, 	0x00001388); 		// writing value 0x00001388 = 5000 = 0.0 to address 20 = 0x28(DMAX)
	TMC_SPIWriteInt(0x2A, 	0x00000578); 		// writing value 0x00000578 = 1400 = 0.0 to address 21 = 0x2A(D1)
	TMC_SPIWriteInt(0x2B, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 22 = 0x2B(VSTOP)
	TMC_SPIWriteInt(0x2C, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 23 = 0x2C(TZEROWAIT)
	TMC_SPIWriteInt(0x2D, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 24 = 0x2D(XTARGET)
	TMC_SPIWriteInt(0x33, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 25 = 0x33(VDCMIN)
	TMC_SPIWriteInt(0x34, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 26 = 0x34(SW_MODE)
	TMC_SPIWriteInt(0x38, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 27 = 0x38(ENCMODE)
	TMC_SPIWriteInt(0x39, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 28 = 0x39(X_ENC)
	TMC_SPIWriteInt(0x3A, 	0x00010000); 		// writing value 0x00010000 = 65536 = 0.0 to address 29 = 0x3A(ENC_CONST)
	TMC_SPIWriteInt(0x3D, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 30 = 0x3D(ENC_DEVIATION)
	TMC_SPIWriteInt(0x60, 	0xAAAAB554); 		// writing value 0xAAAAB554 = 0 = 0.0 to address 31 = 0x60(MSLUT[0])
	TMC_SPIWriteInt(0x61, 	0x4A9554AA); 		// writing value 0x4A9554AA = 1251300522 = 0.0 to address 32 = 0x61(MSLUT[1])
	TMC_SPIWriteInt(0x62, 	0x24492929); 		// writing value 0x24492929 = 608774441 = 0.0 to address 33 = 0x62(MSLUT[2])
	TMC_SPIWriteInt(0x63, 	0x10104222); 		// writing value 0x10104222 = 269500962 = 0.0 to address 34 = 0x63(MSLUT[3])
	TMC_SPIWriteInt(0x64, 	0xFBFFFFFF); 		// writing value 0xFBFFFFFF = 0 = 0.0 to address 35 = 0x64(MSLUT[4])
	TMC_SPIWriteInt(0x65, 	0xB5BB777D); 		// writing value 0xB5BB777D = 0 = 0.0 to address 36 = 0x65(MSLUT[5])
	TMC_SPIWriteInt(0x66, 	0x49295556); 		// writing value 0x49295556 = 1227445590 = 0.0 to address 37 = 0x66(MSLUT[6])
	TMC_SPIWriteInt(0x67, 	0x00404222); 		// writing value 0x00404222 = 4211234 = 0.0 to address 38 = 0x67(MSLUT[7])
	TMC_SPIWriteInt(0x68, 	0xFFFF8056); 		// writing value 0xFFFF8056 = 0 = 0.0 to address 39 = 0x68(MSLUTSEL)
	TMC_SPIWriteInt(0x69, 	0x00F70000); 		// writing value 0x00F70000 = 16187392 = 0.0 to address 40 = 0x69(MSLUTSTART)
	TMC_SPIWriteInt(0x6C, 	0x00410153); 		// writing value 0x00410153 = 4260179 = 0.0 to address 41 = 0x6C(CHOPCONF)
	TMC_SPIWriteInt(0x6D, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 42 = 0x6D(COOLCONF)
	TMC_SPIWriteInt(0x6E, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 43 = 0x6E(DCCTRL)
	TMC_SPIWriteInt(0x70, 	0xC40C001E); 		// writing value 0xC40C001E = 0 = 0.0 to address 44 = 0x70(PWMCONF)

//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^//

}

void TMC_SPIHadle_Int(uint8_t reg, int32_t spi_send_data){
				  spi_del_flag=1;
	
					TMC_SPIReadInt(reg, spi_send_data);				 
					 	//spi接收处理部分
					if(spi_del_flag){
						delay_ms(500);
						 //获取到反馈信息
						if(spi_getptr >= 4){ 
							//处理spi反馈信息
							if(((Spi_receive_buf[0])& 0x10	) != 0 )//目标位置已到达	
								{							
										 Uart2SendStr("Pos OK") ;	 //485串口2输出数据 可以下发指令
								}
							 printf("%s",Spi_receive_buf);	
					     spi_getptr =0;
							 memset(Spi_receive_buf,0,sizeof(Spi_receive_buf));
							 spi_del_flag=0;
					  }
					}

}

void test_Init(void){
//====================================================================================================//
// ACTUAL SETTINGS FOR TMC5160 (created: 2021/10/15 10:30:30)                                        //
//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv//

//	TMC_SPIWriteInt(0x20, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 13 = 0x20(RAMPMODE)
//	TMC_SPIWriteInt(0x24, 	0x000003E8); 		// writing value 0x000003E8 = 1000 = 0.0 to address 16 = 0x24(A1)
//	TMC_SPIWriteInt(0x25, 	0x0000c350); 		// writing value 0x00000000 = 0 = 0.0 to address 17 = 0x25(V1)
//	TMC_SPIWriteInt(0x26, 	0x000001F4); 		// writing value 0x000001F4 = 500 = 0.0 to address 18 = 0x26(AMAX)
//	TMC_SPIWriteInt(0x27, 	0x00002710); 		// writing value 0x00002710 = 10000 = 0.0 to address 19 = 0x27(VMAX)
//	TMC_SPIWriteInt(0x28, 	0x000001F4); 		// writing value 0x000001F4 = 500 = 0.0 to address 20 = 0x28(DMAX)
//	TMC_SPIWriteInt(0x2A, 	0x00000578); 		// writing value 0x00000578 = 1400 = 0.0 to address 21 = 0x2A(D1)
//	TMC_SPIWriteInt(0x2B, 	0x0000000A); 		// writing value 0x0000000A = 10 = 0.0 to address 22 = 0x2B(VSTOP)
//	
  TMC_SPIWriteInt( TMC5160_GCONF, 		0x00000004); 	
	TMC_SPIWriteInt( TMC5160_CHOPCONF, 	0x000100c3);	
	TMC_SPIWriteInt( TMC5160_IHOLD_IRUN, 0x00070300);	
	TMC_SPIWriteInt( TMC5160_PWMCONF, 	0xC40C261E);	
	
	TMC_SPIWriteInt( TMC5160_XTARGET, 0); 	
	TMC_SPIWriteInt( TMC5160_XACTUAL  , 	0x00000000);
	TMC_SPIWriteInt( TMC5160_VACTUAL  , 	0x00000000);
	TMC_SPIWriteInt( TMC5160_VSTART   , 	5);
	TMC_SPIWriteInt( TMC5160_A1       , 	1000);
	TMC_SPIWriteInt( TMC5160_V1       , 	0);			//只用AMAX，DMAX	
	
	TMC_SPIWriteInt( TMC5160_DMAX     , 	5000);
	TMC_SPIWriteInt( TMC5160_D1       , 	1400);
	TMC_SPIWriteInt( TMC5160_VSTOP    , 	10);
	TMC_SPIWriteInt( TMC5160_TZEROWAIT, 	1000);				
	TMC_SPIWriteInt( TMC5160_AMAX     , 	5000);
//	
//	

//	//定位模式,转一圈	
//	TMC_SPIReadInt(motor,TMC5160_CHOPCONF);
//	TMC_SPIWriteInt(motor, TMC5160_XTARGET, 512000); 
//	TMC_SPIWriteInt(motor,TMC5160_RAMPMODE , 	0);
//	TMC_SPIWriteInt(motor,TMC5160_VMAX     , 	20000);
//	
//	delay_ms(5000);	
//	TMC_SPIWriteInt(motor, TMC5160_XTARGET, -51200); 
//	TMC_SPIWriteInt(motor,TMC5160_RAMPMODE , 	0);
//	TMC_SPIWriteInt(motor,TMC5160_VMAX     , 	20000);

//	//定位模式停止
//	TMC_SPIWriteInt(motor, TMC5160_VMAX , 	0);	
//	TMC_SPIWriteInt(motor, TMC5160_XTARGET, 0); 
//	TMC_SPIWriteInt(motor, TMC5160_XACTUAL, 0); //务必TMC5160_XACTUAL == TMC5160_XTARGET
//	
//	TMC_SPIWriteInt(motor, TMC5160_VMAX , 	0);	
//	//TMC_SPIWriteInt(motor, TMC5160_XTARGET, TMC_SPIReadInt(motor,TMC5160_XACTUAL));


	


//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^//

}

void spi_test2(void){
	//====================================================================================================//
// ACTUAL SETTINGS FOR TMC5160 (created: 2021/10/26 10:35:11)                                        //
//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv//
	TMC_SPIWriteInt(0x00, 	0x00000000); 		// writing value 0x00000008 = 8 = 0.0 to address 0 = 0x00(GCONF)
	TMC_SPIWriteInt(0x01, 	0x00000000); 		// writing value 0x00000008 = 8 = 0.0 to address 0 = 0x00(GCONF)
	TMC_SPIWriteInt(0x03, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 1 = 0x03(SLAVECONF)
	TMC_SPIWriteInt(0x84, 	0x00000000); 		// writing value 0x00000008 = 8 = 0.0 to address 0 = 0x00(GCONF)
	TMC_SPIWriteInt(0x05, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 2 = 0x05(X_COMPARE)
	TMC_SPIWriteInt(0x06, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 3 = 0x06(OTP_PROG)
//	TMC_SPIWriteInt(0x08, 	0x0000000D); 		// writing value 0x0000000D = 13 = 0.0 to address 4 = 0x08(FACTORY_CONF)
	TMC_SPIWriteInt(0x09, 	0x00010606); 		// writing value 0x00010606 = 67078 = 0.0 to address 5 = 0x09(SHORT_CONF)
	TMC_SPIWriteInt(0x0A, 	0x00080400); 		// writing value 0x00080400 = 525312 = 0.0 to address 6 = 0x0A(DRV_CONF)
	TMC_SPIWriteInt(0x0B, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 7 = 0x0B(GLOBAL_SCALER)
	TMC_SPIWriteInt(0x10, 	0x00070a03); 		// writing value 0x00070A03 = 461315 = 0.0 to address 8 = 0x10(IHOLD_IRUN)
	TMC_SPIWriteInt(0x11, 	0x0000000A); 		// writing value 0x0000000A = 10 = 0.0 to address 9 = 0x11(TPOWERDOWN)
	TMC_SPIWriteInt(0x13, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 10 = 0x13(TPWMTHRS)
	TMC_SPIWriteInt(0x14, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 11 = 0x14(TCOOLTHRS)
	TMC_SPIWriteInt(0x15, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 12 = 0x15(THIGH)
	TMC_SPIWriteInt(0x20, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 13 = 0x20(RAMPMODE)
	TMC_SPIWriteInt(0x21, 	0x00000000); 		// writing value 0x00000001 = 1 = 0.0 to address 14 = 0x21(XACTUAL)
	TMC_SPIWriteInt(0x23, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 15 = 0x23(VSTART)
	TMC_SPIWriteInt(0x24, 	0x00000000); 		// writing value 0x00001388 = 5000 = 0.0 to address 16 = 0x24(A1)
	TMC_SPIWriteInt(0x25, 	0x00000000); 		// writing value 0x00001388 = 5000 = 0.0 to address 17 = 0x25(V1)
	TMC_SPIWriteInt(0x26, 	0x00000000); 		// writing value 0x00001388 = 5000 = 0.0 to address 18 = 0x26(AMAX)
	TMC_SPIWriteInt(0x27, 	0x00010000); 		// writing value 0x000186A0 = 100000 = 0.0 to address 19 = 0x27(VMAX)
	TMC_SPIWriteInt(0x28, 	0x00000000); 		// writing value 0x00001388 = 5000 = 0.0 to address 20 = 0x28(DMAX)
	TMC_SPIWriteInt(0x2A, 	0x00000000); 		// writing value 0x00000578 = 1400 = 0.0 to address 21 = 0x2A(D1)
	TMC_SPIWriteInt(0x2B, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 22 = 0x2B(VSTOP)
	TMC_SPIWriteInt(0x2C, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 23 = 0x2C(TZEROWAIT)
	TMC_SPIWriteInt(0x2D, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 24 = 0x2D(XTARGET)
	TMC_SPIWriteInt(0x33, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 25 = 0x33(VDCMIN)
	TMC_SPIWriteInt(0x34, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 26 = 0x34(SW_MODE)
TMC_SPIWriteInt(0x35, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 26 = 0x34(SW_MODE)	
	TMC_SPIWriteInt(0x38, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 27 = 0x38(ENCMODE)
	TMC_SPIWriteInt(0x39, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 28 = 0x39(X_ENC)
	TMC_SPIWriteInt(0x3A, 	0x00010000); 		// writing value 0x00010000 = 65536 = 0.0 to address 29 = 0x3A(ENC_CONST)
	TMC_SPIWriteInt(0x3D, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 30 = 0x3D(ENC_DEVIATION)
	TMC_SPIWriteInt(0x60, 	0xAAAAB554); 		// writing value 0xAAAAB554 = 0 = 0.0 to address 31 = 0x60(MSLUT[0])
	TMC_SPIWriteInt(0x61, 	0x4A9554AA); 		// writing value 0x4A9554AA = 1251300522 = 0.0 to address 32 = 0x61(MSLUT[1])
	TMC_SPIWriteInt(0x62, 	0x24492929); 		// writing value 0x24492929 = 608774441 = 0.0 to address 33 = 0x62(MSLUT[2])
	TMC_SPIWriteInt(0x63, 	0x10104222); 		// writing value 0x10104222 = 269500962 = 0.0 to address 34 = 0x63(MSLUT[3])
	TMC_SPIWriteInt(0x64, 	0xFBFFFFFF); 		// writing value 0xFBFFFFFF = 0 = 0.0 to address 35 = 0x64(MSLUT[4])
	TMC_SPIWriteInt(0x65, 	0xB5BB777D); 		// writing value 0xB5BB777D = 0 = 0.0 to address 36 = 0x65(MSLUT[5])
	TMC_SPIWriteInt(0x66, 	0x49295556); 		// writing value 0x49295556 = 1227445590 = 0.0 to address 37 = 0x66(MSLUT[6])
	TMC_SPIWriteInt(0x67, 	0x00404222); 		// writing value 0x00404222 = 4211234 = 0.0 to address 38 = 0x67(MSLUT[7])
	TMC_SPIWriteInt(0x68, 	0xFFFF8056); 		// writing value 0xFFFF8056 = 0 = 0.0 to address 39 = 0x68(MSLUTSEL)
	TMC_SPIWriteInt(0x69, 	0x00F70000); 		// writing value 0x00F70000 = 16187392 = 0.0 to address 40 = 0x69(MSLUTSTART)
	TMC_SPIWriteInt(0x6C, 	0x00410153); 		// writing value 0x00410153 = 4260179 = 0.0 to address 41 = 0x6C(CHOPCONF)
	TMC_SPIWriteInt(0x6D, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 42 = 0x6D(COOLCONF)
	TMC_SPIWriteInt(0x6E, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 43 = 0x6E(DCCTRL)
	TMC_SPIWriteInt(0x70, 	0xC40C001E); 		// writing value 0xC40C001E = 0 = 0.0 to address 44 = 0x70(PWMCONF)

//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
}






