#include "uart3.h"

char wptr3;
char rptr3;
char buffer3[16];
bit busy3;
void Uart3Isr() interrupt 17
{
    if (S3CON & 0x02)
    {
        S3CON &= ~0x02;
        busy3 = 0;
    }
    if (S3CON & 0x01)
    {
        S3CON &= ~0x01;
        buffer3[wptr3++] = S3BUF;
        wptr3 &= 0x0f;
    }
}

void Uart3Init()
{
    S3CON = 0x10;
    T2L = BRT;
    T2H = BRT >> 8;
    AUXR |= 0x14;
    wptr3 = 0x00;
    rptr3 = 0x00;
    busy3 = 0;
}

void Uart3Send(char dat)
{
    while (busy3);
    busy3 = 1;
    S3BUF = dat;
}

void Uart3SendStr(char *p)
{
    while (*p)
    {
        Uart3Send(*p++);
    }
}
