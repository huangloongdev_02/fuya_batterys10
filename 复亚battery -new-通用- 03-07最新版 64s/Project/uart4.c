#include "uart4.h"
#include "source.h"
//串口3使用寄存器
char wptr4 = 0;
char rptr4 = 0;
uchar buffer4[256]={0};
bit busy4;
//新增变量
uchar   Uart4_getcom=0;
uchar   Uart4_get_data[256]={0};
uchar   Uart4_send_data[15]={0};
uint16_t   current_voltage=0;
uchar   current_power=0;
uchar   charging_time = 0;
uchar   uart4_id = 0;
uchar   current_state = 0;
uchar   current_state_bits = 0;
uchar   inspection_data  ;
uchar   get_inspection_data ;
uchar   Open_flag = 0; //飞机开启标志

void Uart4Isr() interrupt 18
{
	int i =0;
	int t=0;
	int j=0;
    if (S4CON & 0x02)
    {
        S4CON &= ~0x02;
        busy4 = 0;
    }
    if (S4CON & 0x01)
    {
//			Uart4_getcom=0;
//uchar   Uart4_get_data[256]={0};

        S4CON &= ~0x01;
        buffer4[wptr4++] = S4BUF;
        wptr4 &= 0x0ff;
			 for(i=0;i<wptr4;i++){
					if(((uint8_t)(buffer4[i])== 0x3c) &&((uint8_t)(buffer4[i+5])== 0xc3)&&(wptr4>=5))				
					{	
						int temp= 0;
						temp = (uchar)buffer4[i]+ (uchar)buffer4[i+1]+ (uchar)buffer4[i+2]+ (uchar)(buffer4[i+3]);
						inspection_data = (uint8_t)(temp & 0xff ); //计算的校验数据
						get_inspection_data = buffer4[i+4];
//						temp = temp & 0xff;
						if( inspection_data == get_inspection_data ){ 						
							for(t=i;t<i+6;t++){       
									Uart4_get_data[j++]=buffer4[t];
								}
							}
//					 Uart4_getcom = 1;
//					 wptr4 = 0;	        
//	         memset(buffer4,0,sizeof(buffer4));
				
					 Uart4_getcom = 1;
					 wptr4 = 0;	        
	         memset(buffer4,0,sizeof(buffer4));
					}
							}
			// }
   }
	
}

void Uart4Init()
{
    S4CON = 0x10;
    T2L = BRT;
    T2H = BRT >> 8;
//    T4T3M = 0xa0;
	  AUXR |= 0x14;
    wptr4 = 0x00;
    rptr4 = 0x00;
    busy4 = 0;
}

void Uart4Send(char dat)
{
    while (busy4);
    busy4 = 1;
    S4BUF = dat;
}

void Uart4SendStr(char *p)
{
    while (*p)
    {
        Uart4Send(*p++);
    }
}


//uart4 deal program
void uart4_rec_deal(void){
 if(Uart4_getcom==1){
							if( (Uart4_get_data[1] == 0x00) || (Uart4_get_data[1] == uart4_id )){
								 
							     if(Uart4_get_data[2] == 0x01){   //充电状态 查询	
                        uart4_inquiry_send_deal() ;   //充电状态回复
										 
									 }else if(Uart4_get_data[2] == 0x02){    //充电命令下发
									    switch (Uart4_get_data[3])
											{
													case 1 :    //关机充电	
          									if(Open_flag == 1)	//飞机开机
														{															
															if( P34 == 1){    //O1  充电 
																 P34 = 0;
																 P27= 0;    //关机 
																 delay_ms(2000);	
															 //  P32 = 0; // 无人机关机	
																 power_control();
																 delay_ms(10000);	
																
															}
															else if( P34 == 0){    //O1  未充电 		
															 
															 //  P32 = 0; // 无人机关机	
																 power_control();													  	
																
															}
														}
														 P27= 1;   //P27   开机
												     P34 = 1;    //O1  充电
													  current_state = 		0x02	;	 //充电模块状态指示	关机充电				
																	 break;     // 并退出 switch
													case 2 :   //开机充电

														//判断无人机是否开机
													if(Open_flag == 0)	//飞机关机
														{															
 														   power_control();
														}
													     P27= 1;   //P27
												       P34 = 1;   
													    current_state = 		0x03	;	 //充电模块状态指示	开机充电			
																	 break;
													case 0x11 :    //停止充电													  												    
												       P34 = 0;
												       P27= 0;
															 current_state = 		0x01	;	 //充电模块状态指示	未充电				
																	 break;     // 并退出 switch
													case 0x12:   //无人机开机
														if(Open_flag == 1)	//飞机开机的情况下
														{
														    if( P34 == 1){    //O1  开机充电时接收到开关机指令   
																	 P27= 0;   //P27
																	 P34 = 0;  
																	 delay_ms(2000);	
																 //  P32 = 0; // 无人机关机	
																	 power_control();
																	 delay_ms(10000);	
																		P27= 1;   //P27
																		P34 = 1;  							
																	}else{
																	  power_control(); //未充电时接收到开关机指令
																	}
													}else {    //飞机关机的情况下，获取到开关机指令
													  power_control();
													}
 														

														if(	current_state != 		0x03	) //充电模块状态指示	开机充电		
														{
														   current_state = 		0x04	;	 //充电模块状态指示	无人机开机					
														}															
																	 break;
													case 0x13:   //无人机关机
														
														//判断一下当无人机开机的时候并且充电时候
													  //先关电源后才能关机
//													if(Open_flag == 1)	//飞机开机
//													{															
															if( P34 == 1){    //O1  充电 
																	 P27= 0;   //P27
																	 P34 = 0;  
																	 delay_ms(2000);	
																 //  P32 = 0; // 无人机关机	
																	 power_control();
																	 delay_ms(10000);	
																		P27= 1;   //P27
																		P34 = 1;  							
														}
													else if( P34 == 0){ //未充电
													  power_control();
													}
//												}
													//power_control();
														if(	current_state != 		0x02	) //充电模块状态指示	关机充电		
														{
														   current_state = 		0x09	;	 //充电模块状态指示	无人机关机							
														}																									
														break;
												  default:
													printf("commmand error!\r\n"); // 命令下发出错
											}
											
											
									 
									 }if(Uart4_get_data[2] == 0x03){  //设置充电板地址
									     uart4_write_id();										 
									 }if(Uart4_get_data[2] == 0x04){  //返回充电板地址
									     uart4_inquiry_send_deal() ;   //充电状态回复
									 
									 }if(Uart4_get_data[2] == 0x05){
									     uart4_write_time_limit();
									 
									 }if(Uart4_get_data[2] == 0x06){
									    Trans_chose = Uart4_get_data[3];//上传是否需要开启
										  uart4_write_Trans_chose();
											//Trans_times = 0;//上传周期设置
									 }if(Uart4_get_data[2] == 0x07){
											Trans_times = Uart4_get_data[3];//上传周期设置
										  uart4_write_Trans_times();
									 }
									 
									 						 
							}
						 UartSendStr(Uart4_get_data);                                                                                                 
						 Uart4_getcom=0;
						 memset(Uart4_get_data,0,sizeof(Uart4_get_data));
						}
}

//uart4 send deal program
void uart4_inquiry_send_deal(void){
	
	int i=0;
	int j=0;
	P43 = 1; //发送使能
	Uart4_send_data[0] = 0x3c;
	Uart4_send_data[1] = 0x01; //查询上报
	//Uart4_send_data[1] = 0x02; //自动上报
	
	Uart4_send_data[2] = uart4_id; //当前设备地址
//	 P32 = 0    ; // 无人机关机
//	 P27= 1;   //P27
//	 P34 = 1;   //O1
	////充电状态	
	 Uart4_send_data[3]  = current_state_bits;

	 Uart4_send_data[4]  = current_voltage >> 8;												//当前电压
	 Uart4_send_data[5]  = current_voltage & 0x00ff;												//当前电压
   Uart4_send_data[6]  = current_power;  //当前电量
	
	Uart4_send_data[7] = charging_time;  //充电时间
	Uart4_send_data[8] = Charging_time_limit;             //充电限制时间
	Uart4_send_data[12] = 0xc3;
	Uart4_send_data[11] = 0; //校验和
	for( j=0;j<11;j++){
		Uart4_send_data[11] += Uart4_send_data[j];
	}
	Uart4_send_data[11] &=0xff;
	 for(i=0;i<13;i++ ){
	    Uart4Send(Uart4_send_data[i]);
	 }
	delay_ms(2)	;
	P43 = 0; //接收使能
}

//uart4 send deal program
void uart4_auto_send_deal(void){
	
	int i=0;
	int j=0;
	P43 = 1; //发送使能
	Uart4_send_data[0] = 0x3c;
//	Uart4_send_data[1] = 0x01; //查询上报
	Uart4_send_data[1] = 0x02; //自动上报
	
	Uart4_send_data[2] = uart4_id; //当前设备地址
//	 P32 = 0    ; // 无人机关机
//	 P27= 1;   //P27
//	 P34 = 1;   //O1
	////充电状态	
	 Uart4_send_data[3]  = current_state_bits;
//  Uart4_send_data[3]  = 0x20;
	 Uart4_send_data[4]  = current_voltage >> 8;												//当前电压
	 Uart4_send_data[5]  = current_voltage & 0x00ff;												//当前电压
   Uart4_send_data[6]  = current_power;  //当前电量
	
	Uart4_send_data[7] = charging_time;  //充电时间
	Uart4_send_data[8] = Trans_chose; //是否上报
	Uart4_send_data[9] = Trans_times; //上报周期
	Uart4_send_data[10] = Charging_time_limit; //设置的充电时间
	Uart4_send_data[12] = 0xc3;
	Uart4_send_data[11] = 0; //
	for( j=0;j<11;j++){
		Uart4_send_data[11] += Uart4_send_data[j];
	}
	Uart4_send_data[11] &=0xff;
	 for(i=0;i<13;i++ ){
	    Uart4Send(Uart4_send_data[i]);
	 }
	delay_ms(2)	;
	P43 = 0; //接收使能
	
}


void  uart4_write_id(){
       
			IapErase(0x7E00);    //清除512个字节 
	    IapProgram(0x7E01,Uart4_get_data[3] );//写入值 到地址0x7E01
      uart4_id = (uchar)IapRead(0x7E01);      //读取 地址0x7e01 的值 
}	


//读 id
void  uart4_id_read(){
      uart4_id = (uchar)IapRead(0x7E01);      //读取 地址0x7e01 的值
  //    printf("uart4_id =  %x\r\n",(int)uart4_id );
	
}
void  uart4_write_time_limit(){    
			IapErase(0x7c00);    //清除512个字节 
	    IapProgram(0x7c01,Uart4_get_data[3] );//写入值 
      Charging_time_limit = (uchar)IapRead(0x7c01);      //读取 地址0x7c01 的值 
}	


//读 time
void  uart4_read_time_limit(){
      Charging_time_limit = (uchar)IapRead(0x7c01);      //读取 地址0x7c01 的值
}
//是否上传选择
void  uart4_write_Trans_chose(){    
			IapErase(0x7A00);    //清除512个字节 
	    IapProgram(0x7A01,Uart4_get_data[3]);//写入值 
      Trans_chose = (uchar)IapRead(0x7A01);      //读取 地址0x7c01 的值 
}	


//读
void  uart4_read_Trans_chose(){
     Trans_chose = (uchar)IapRead(0x7A01);      //读取 地址0x7c01 的值
}

//s
void  uart4_write_Trans_times(){    
			IapErase(0x7800);    //清除512个字节 
	    IapProgram(0x7801,Uart4_get_data[3]); //写入值 
      Trans_times = (uchar)IapRead(0x7801); //读取 地址0x7c01 的值 
}	


//读
void  uart4_read_Trans_times(){
     Trans_times = (uchar)IapRead(0x7801);      //读取 地址0x7c01 的值
}
void  power_control(void){
   P32 = 1    ; // 无人机开关机	
   delay_ms(500);	
   P32 = 0    ; // 无人机开关机	
	 delay_ms(500);	
   P32 = 1   ; // 无人机开关机	
	 delay_1000ms(3);
   P32 = 0  ; // 无人机开关机	
}

//充电状态检测
void get_current_state_bit0(void){
	          if(P34 == 0){            
					     current_state_bits  &=  ~0x01 ;    //未充电 
						}
						if(P34 == 1){
							 current_state_bits  |= 0x01;    //充电
						}
}

////电池状态检测
//void get_current_state_bit(void){
//	          if(P34 == 0){            
//					     current_state_bits  &=  !0x01 ;    //未充电 
//						}
//						if(P34 == 1){
//							 current_state_bits  |= 0x01;    //充电 
//						}
//}


//自动上传数据选择并且设定上传时间
void uart4_auto_send_chose_timeset(void){
	
	
}







