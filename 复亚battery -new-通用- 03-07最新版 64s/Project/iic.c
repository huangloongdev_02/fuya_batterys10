#include "iic.h"
void Wait()
{
    while (!(I2CMSST & 0x40));   //完成MSCMD命令后产生的中断信号
    I2CMSST &= ~0x40;
}

void Start()
{
    I2CMSCR = 0x01;                             //发送START命令
    Wait();
}

void SendData(char dat)
{
    I2CTXD = dat;                               //写数据到数据缓冲区
    I2CMSCR = 0x02;                             //发送SEND命令
    Wait();
}

void RecvACK()
{
    I2CMSCR = 0x03;                             //发送读ACK命令
    Wait();
}

char RecvData()
{
    I2CMSCR = 0x04;                             //发送RECV命令
    Wait();
    return I2CRXD;
}

void SendACK()
{
    I2CMSST = 0x00;                             //设置ACK信号
    I2CMSCR = 0x05;                             //发送ACK命令
    Wait();
}

void SendNAK()
{
    I2CMSST = 0x01;                             //设置NAK信号
    I2CMSCR = 0x05;                             //发送ACK命令
    Wait();
}

void Stop()
{
    I2CMSCR = 0x06;                             //发送STOP命令
    Wait();
}

void Delay()
{
    int i;

    for (i=0; i<3000; i++)
    {
        _nop_();
        _nop_();
        _nop_();
        _nop_();
    }
}

//**************************************
//初始化MPU6050
//**************************************
//void InitMPU6050()
//{
//	Single_WriteI2C(PWR_MGMT_1, 0x00);	//解除休眠状态
//	Single_WriteI2C(SMPLRT_DIV, 0x07);
//	Single_WriteI2C(CONFIG, 0x06);
//	Single_WriteI2C(GYRO_CONFIG, 0x18);
//	Single_WriteI2C(ACCEL_CONFIG, 0x01);
//}

//**************************************
//向I2C设备写入一个字节数据
//**************************************
//void Single_WriteI2C(uchar REG_Address,uchar REG_data)
//{
//    Start();                  //起始信号
//    SendData(SlaveAddress);   //发送设备地址+写信号
//	  RecvACK();
//    SendData(REG_Address);    //内部寄存器地址，
//		RecvACK();
//    SendData(REG_data);       //内部寄存器数据，
//	  RecvACK();
//    Stop();                   //发送停止信号
//}

//**************************************
//从I2C设备读取一个字节数据
//**************************************
//unsigned char Single_ReadI2C(unsigned char REG_Address)
//uchar Single_ReadI2C(uchar REG_Address)
//{
//    uchar REG_data;
//	  Start();                 //起始信号
//    SendData(SlaveAddress);    //发送设备地址+写信号
//		RecvACK();
//		SendData(REG_Address);     //发送存储单元地址，从0开始	
//		RecvACK();                  
//		Start();
//	
//		SendData(SlaveAddress+1);  //发送设备地址+读信号
//    RecvACK();
//   	REG_data=RecvData();       //读出寄存器数据
//		SendNAK();
//		Stop();
////	I2C_SendACK(1);                //接收应答信号
////	I2C_Stop();                    //停止信号
//   	return REG_data;
//}

//**************************************
//读取数据
//**************************************
// int GetData(uchar REG_Address)
//{
//	char H,L;
//	H=Single_ReadI2C(REG_Address);
//	L=Single_ReadI2C(REG_Address+1);
//	return (H<<8)+L;   //合成数据
//}
