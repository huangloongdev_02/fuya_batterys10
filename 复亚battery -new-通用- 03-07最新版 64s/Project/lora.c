#include "lora.h"

uint8_t report_has_reply = 0;

//static uint8_t __recv_array[LORA_MAX_BUFFER_LENGTH] = {0};
//static LoraBuffer __recv_buf_struct = lorabuffer_static_init(__recv_array, LORA_MAX_BUFFER_LENGTH);
//LoraBuffer* __rbuf = &__recv_buf_struct;

LORAPredefinedCommand _module_version = {"ATI", 10000};
LORAPredefinedCommand _id = {"AT+ID?", 10000};
LORAPredefinedCommand _addr = {"AT+ADDR?", 20000};
LORAPredefinedCommand _cfg = {"AT+CFG?", 5000};
LORAPredefinedCommand _setip = {"AT+SIP=01", 5000};

char lora_config[] = "+++";
char lora_config_end[] = "ATT\r\n";
char lora_ver[] = "ATI\r\n";
char lora_help[] = "ATH\r\n";
char lora_id[] = "AT+ID?\r\n";
char lora_addr[] = "AT+ADDR?\r\n";
char lora_cfg[] = "AT+CFG?\r\n";
//char lora_setip[] = "AT+SIP=00\r\n";  //设置序列号打开协议
char lora_setip[] = "AT+SIP=01\r\n";  //设置序列号打开协议
char lora_settsf[] = "AT+TSF=0C\r\n"; 
char lora_setlcp[] = "AT+LCP=0000\r\n";
char lora_setrprem[] = "AT+RPREM=0008\r\n";
char lora_setfnbon[] = "AT+FNB=8f\r\n"; 
char lora_setfnboff[] = "AT+FNB=00\r\n";
char lora_setpow[] = "AT+POW=14\r\n";
char lora_bate_9600[] = "AT+BRATE=03\r\n"; //设置波特率
char lora_bate_115200[] = "AT+BRATE=07\r\n";
char lora_bate_230400[] = "AT+BRATE=08\r\n";

//init lora

//lora to lora
char lora_ntn_mod[] = "AT+NET=00\r\n";
char lora_ntn_tfreq[] = "AT+TFREQ=19CF0E40\r\n";
char lora_ntn_rfreq[] = "AT+RFREQ=19CF0E40\r\n";
//char lora_ntn_rfreq[] = "AT+RFREQ=1E3093A0\r\n";配置接收频率 506.5MHz

char lora_ntn_riq[] = "AT+RIQ=00\r\n";  //关闭模块接收反转
char lora_ntn_cfg[] = "AT+CFG?\r\n";

//点到点模式
LORAAtResult lora_ntn_regsiter(){
	 LORAAtResult result;
	 Lora_send_command("ATZ\r\n");//恢复默参数	
	
	 Lora_send_command("ATI\r\n"); //查询版本号	
	 delay_ms(1000);
	 lora_dispalyRead();
//	result = Lora_send_command(lora_config);   //lora进入配置模式
//  delay_ms(1000);
//lora_dispalyRead();
	result = Lora_send_command(lora_ntn_mod);   //将 NET 选项配置到 Node to Node 模式
  delay_ms(1000);
	lora_dispalyRead();
//	result = Lora_send_command(lora_setip);  //设置ip和协议功能
//  delay_ms(1000);
	result = Lora_send_command("AT+SIP=00\r\n");//设置序号及协议功能默认00
	delay_ms(1000);
	lora_dispalyRead();
	result = Lora_send_command(lora_ntn_tfreq);   //配置发送频率 433MHz
  delay_ms(1000);
	lora_dispalyRead();
	result = Lora_send_command(lora_ntn_rfreq);   //配置接收频率 433MHz
  delay_ms(1000);
	lora_dispalyRead();
	result = Lora_send_command(lora_ntn_riq);   //关闭模块接收反转
  delay_ms(1000);
	lora_dispalyRead();
	result = Lora_send_command(lora_ntn_cfg);   //查询配置后的信息
  delay_ms(2000);
	lora_dispalyRead();
//	result = Lora_send_command(lora_config_end);   //配置结束
//  delay_ms(1000);
//	lora_dispalyRead();
	return result;
}


LORAAtResult lora_chose_net_mode(){
    LORAAtResult result;
		P12=1; //AT引脚 
				
		delay_1000ms(1);	
		lora_regsiter(); //lora网络通讯模式
			 
		P12=0; //AT引脚 
		return result;
}
LORAAtResult lora_chose_ntn_mode(){	
	  LORAAtResult result;
    P12=1; //AT引脚 
		//chose_mode = NTN; //模式选择点到点模式
		delay_1000ms(1);	
		lora_ntn_regsiter(); //点到点通讯模式		
	
		P12=0; //AT引脚 	
		return result;

}




LORAAtResult Lora_send_command(char *s){
	//memset(buffer2,0,sizeof(buffer2));	
	//wptr2 = 0;
  printf("Send to lora command:%s",s);
	Uart2SendStr(s);
  return LORA_AT_OK;
}



void  lora_dispalyRead(){
			 printf("lora_read:%s",buffer2);
  		 printf("\r\n");
			 wptr2 = 0;	        
	     memset(buffer2,0,sizeof(buffer2));
}	

//
void  lora_ntn_dispalyRead(){
	     printf("lora_ntn write id success!\r\n");			
//		 printf("lora_read:%s",buffer2);
//  	 printf("\r\n");
			 wptr2 = 0;	        
	     memset(buffer2,0,sizeof(buffer2));
}	




//串口1写地址
void  lora_uart_write_id(){
       
			IapErase(0x7E00);    //清除512个字节 
	    printf("BUFFER[1] = %X",buffer[1]);
			printf("\r\n");

	    IapProgram(0x7E01, buffer[1]);//写入值 到地址0x7E01
			IapProgram(0x7E02, buffer[2]);//写入值 到地址0x7E02
			IapProgram(0x7E03, buffer[3]);//写入值 到地址0x7E03
			IapProgram(0x7E04, buffer[4]);//写入值 到地址0x7E04
      mcu_read_id(); 
			wptr = 0;	        
	    memset(buffer,0,sizeof(buffer));
}	

void  uart_write_mv_acc(){
       
			IapErase(0x7C00);    //清除512个字节 用来存放mv acc
	    printf("BUFFER[1] = %X",buffer[1]);
			printf("\r\n");

	    IapProgram(0x7C01, buffer[1]);//写入值 到地址0x7c01
			IapProgram(0x7C02, buffer[2]);//写入值 到地址0x7c02
			IapProgram(0x7C03, buffer[3]);//写入值 到地址0x7c03
			IapProgram(0x7C04, buffer[4]);//写入值 到地址0x7c04
      mcu_read_mv_acc(); 
			wptr = 0;	        
	    memset(buffer,0,sizeof(buffer));
}

//读MV_acc
void  mcu_read_mv_acc(){
      eeprom_read5 = (uchar)IapRead(0x7c01);      //读取 地址0x7c01 的值 
	    eeprom_read6 = (uchar)IapRead(0x7c02);      //读取 地址0x7c02 的值 
			eeprom_read7 = (uchar)IapRead(0x7c03);      //读取 地址0x7c03 的值 
	    eeprom_read8 = (uchar)IapRead(0x7c04);      //读取 地址0x7c04 的值 
	
//      printf("eeprom_read5 = %x\r\n",(int)eeprom_read5);
//	    printf("eeprom_read6 = %x\r\n",(int)eeprom_read6);
//			printf("eeprom_read7 = %x\r\n",(int)eeprom_read7);
//			printf("eeprom_read8 = %x\r\n",(int)eeprom_read8);
}

//读 id
void  mcu_read_id(){
      eeprom_read1 = (uchar)IapRead(0x7E01);      //读取 地址0x7e01 的值 
	    eeprom_read2 = (uchar)IapRead(0x7E02);      //读取 地址0x7e02 的值 
			eeprom_read3 = (uchar)IapRead(0x7E03);      //读取 地址0x7e03 的值 
	    eeprom_read4 = (uchar)IapRead(0x7E04);      //读取 地址0x7e04 的值 
	
//      printf("eeprom_read1 = %x\r\n",(int)eeprom_read1);
//	    printf("eeprom_read2 = %x\r\n",(int)eeprom_read2);
//			printf("eeprom_read3 = %x\r\n",(int)eeprom_read3);
//			printf("eeprom_read4 = %x\r\n",(int)eeprom_read4);
}
void mcu_get_stand_mv_acc(){
     mcu_read_mv_acc();  //注意只有调用后eeprom_read5 6 7 8 才有值 不要忘记调用
		 if((eeprom_read5 != 0xff) ||(eeprom_read6!= 0xff)){
		 mid_mv = eeprom_read5 << 8;
		 mid_mv |=  eeprom_read6;
		 mv_transf_v = mid_mv/1000.0;
		 }else
		 {
			mv_transf_v = usual_v;
		 }
		 printf("mv_transf_v = %f\r\n",mv_transf_v);
		 if((eeprom_read7 != 0xff)){
		 mid_acc = eeprom_read7;
		 write_acc_transf_standard = mid_acc / 10.0;
		 }else{
		  write_acc_transf_standard = usual_acc;
		 }
		 printf("write_acc_transf_standard = %f\r\n",write_acc_transf_standard);
}

// lora_chose_ntn_mode();
//				if((buffer2[0] ==0x53) && (buffer2[6]==0x72)){
//					//lora_dispalyRead();
//					lora_write_id();	
//					printf("lora_ntn write id success!\r\n");					
//					lora_ntn_dispalyRead();
//					led_speed = 1;
//				}else{
//					 wptr2 = 0;	 
//					 mcu_read_id();
//					printf("lora_ntn write id error!\r\n");		
//			}

//lora write id
void  lora_write_id(){
	   if((buffer2[0] ==0x53) && (buffer2[6]==0x72)){
			IapErase(0x7E00);    //清除512个字节 
	    printf("BUFFER2[1] = %d",(int)buffer2[1]);
			printf("\r\n");

	    IapProgram(0x7E01, buffer2[1]);//写入值 到地址0x7E01
			IapProgram(0x7E02, buffer2[2]);//写入值 到地址0x7E02
			IapProgram(0x7E03, buffer2[3]);//写入值 到地址0x7E03
			IapProgram(0x7E04, buffer2[4]);//写入值 到地址0x7E04
	
      mcu_read_id(); 
//			printf("lora_read:%s",buffer2);
//  		printf("\r\n");
			wptr2 = 0;	        
	    memset(buffer2,0,sizeof(buffer2));
			printf("lora_ntn write id success!\r\n");	
   //   led_speed = 1;				 
			//		lora_ntn_dispalyRead();
					
			}else{	 
					 mcu_read_id();
				   wptr2 = 0;	        
	         memset(buffer2,0,sizeof(buffer2));
					printf("lora_ntn write id error!\r\n");		
			}
}

void  lora_send_net_data(){
	   uint32_t id_addr = 0;
	  // 	wptr2 = 0;	        
	 //   memset(buffer2,0,sizeof(buffer2));
			eeprom_read1 = (uchar)IapRead(0x7E01);      //读取 地址0x7e01 的值 
	    eeprom_read2 = (uchar)IapRead(0x7E02);      //读取 地址0x7e02 的值 
			eeprom_read3 = (uchar)IapRead(0x7E03);      //读取 地址0x7e03 的值 
	    eeprom_read4 = (uchar)IapRead(0x7E04);      //读取 地址0x7e04 的值 
	    meassage_num ++;
	
	    meassage_num &= 0xFF;
	 
	    Uart2Send(start_1);
	    Uart2Send(start_2);
	    Uart2Send(meassage_num);
			Uart2Send(eeprom_read4);
			Uart2Send(eeprom_read3);
			Uart2Send(eeprom_read2);
			Uart2Send(eeprom_read1);
			Uart2Send(power);
	    Uart2Send(state_data);
	    Uart2Send(reserved1);
			Uart2Send(reserved2);
			Uart2Send(reserved3);
			Uart2Send(reserved4);		
			Uart2Send(end_1);
			Uart2Send(end_2);
			id_addr = (uint32_t)eeprom_read4 + ((uint32_t)eeprom_read3) * 16 *16 + ((uint32_t)eeprom_read2)*16*16*16*16 + ((uint32_t)eeprom_read1)*16*16*16*16*16*16;
			printf("id_addr = %lu\r\n",id_addr);
		//	printf("ID_addr = %ld\r\n",);
			printf("meassage = %X\r\n",(int)meassage_num);
}
//发送网关碰撞没有收到的重要数据
void  lora_send_net_data_important(){
	 // 	 wptr2 = 0;	        
	 //    memset(buffer2,0,sizeof(buffer2));
			eeprom_read1 = (uchar)IapRead(0x7E01);      //读取 地址0x7e01 的值 
	    eeprom_read2 = (uchar)IapRead(0x7E02);      //读取 地址0x7e02 的值 
			eeprom_read3 = (uchar)IapRead(0x7E03);      //读取 地址0x7e03 的值 
	    eeprom_read4 = (uchar)IapRead(0x7E04);      //读取 地址0x7e04 的值 
	    meassage_num ++;
	
	    meassage_num &= 0xFF;
	
	    Uart2Send(start_1);
	    Uart2Send(start_2);
	    Uart2Send(meassage_num);
		  Uart2Send(eeprom_read4);
			Uart2Send(eeprom_read3);
			Uart2Send(eeprom_read2);
			Uart2Send(eeprom_read1);
			Uart2Send(power);
	    Uart2Send(0x01);
	    Uart2Send(reserved1);
			Uart2Send(reserved2);
			Uart2Send(reserved3);
			Uart2Send(reserved4);		
			Uart2Send(end_1);
			Uart2Send(end_2);
}

//lora网络初始化部分
LORAAtResult lora_regsiter(){

	LORAAtResult result;
			Lora_send_command("ATZ\r\n");//恢复默参数		
			delay_ms(2000);
	  	lora_dispalyRead();
			result = Lora_send_command("AT+NET=01\r\n");//设置网络模式  00通常用于点对点通信，01跳频用于基站
			delay_ms(1000);
			lora_dispalyRead();
//	lora_set_bate();
//	lora_uart_reset_init();
//	rt_pin_mode(LORA_POWER_PIN,PIN_MODE_OUTPUT);
	//rt_pin_write(LORA_POWER_PIN, PIN_HIGH);
	//rt_pin_write(LORA_AT_PIN, PIN_HIGH);
//	delay_ms(1000);
	 Lora_send_command("ATI\r\n"); //查询版本号	
	 delay_ms(1000);
	 lora_dispalyRead();
//	result = Lora_send_command(lora_ver); //查询版本号
//	delay_ms(1000);
//  lora_dispalyRead();
	result = Lora_send_command(lora_setip);  //设置ip和协议功能
  delay_ms(1000);
//	while((strcmp(buffer2,"\r\nOK\r\n")))
//	{
//		printf("while1");
//		result = Lora_send_command(lora_setip);
//		delay_ms(1000);
//	  lora_dispalyRead();
//	}
 // printf("test---------------------------------");
	result = Lora_send_command(lora_id);   //查询设备iD
  delay_ms(1000);
	lora_dispalyRead();	
	result = Lora_send_command(lora_settsf);//设置扩频因子 12
	delay_ms(1000);
	result = Lora_send_command(lora_addr); //查询单播地址
	delay_ms(1000);
	Lora_send_command(lora_setrprem);	//设置前导码值
	delay_ms(1000);
	result = Lora_send_command(lora_setlcp); //设置链路检测周期
	delay_ms(1000);
	result = Lora_send_command(lora_setpow); //设置发射功率
	delay_ms(1000);
	result = Lora_send_command(lora_setfnbon); //设置退避策略
	delay_ms(1000);
	result = Lora_send_command(lora_cfg);  //查询接收配置参数
	delay_ms(1000);
//rt_pin_write(LORA_AT_PIN, PIN_LOW);
	return result;
}



//未使用
//LORAAtResult lora_net_regsiter(){
//	 LORAAtResult result;
//		Lora_send_command("ATZ\r\n");//恢复默参数		
//		delay_ms(2000);
//		lora_dispalyRead();
//	
//	 Lora_send_command("AT+ADDR?\r\n"); //查询模块地址指令
//	 delay_ms(1000);
//	 lora_dispalyRead();
//	 Lora_send_command("AT+SIP=01\r\n"); //打开节点模块地址功能指令
//	 delay_ms(1000);
//	 lora_dispalyRead();
//	
//	
//	 return result;
//}

//mycode  未使用
LORAAtResult lora_init_regsiter(){
	LORAAtResult result;
	result = Lora_send_command("AT+NET=00\r\n");//设置网络模式  00通常用于点对点通信，01跳频用于基站
	delay_ms(1000);
	lora_dispalyRead();
	result = Lora_send_command(lora_ntn_tfreq);   //配置发送频率 433MHz
	delay_ms(1000);
	lora_dispalyRead();
	result = Lora_send_command(lora_ntn_rfreq);   //配置接收频率 506.5MHz
  delay_ms(1000);
	lora_dispalyRead();
	result = Lora_send_command(lora_setpow); //设置发射功率
	delay_ms(1000);
	lora_dispalyRead();
	result = Lora_send_command("AT+BW=07\r\n"); //设置带宽125K
	delay_ms(1000);
	lora_dispalyRead();
	result = Lora_send_command(lora_settsf);//设置发送扩频因子 12
	delay_ms(1000);
	lora_dispalyRead();
	result = Lora_send_command("AT+RSF=0C\r\n");//设置接收扩频因子 12
	delay_ms(1000);
	lora_dispalyRead();
	result = Lora_send_command("AT+CR=01\r\n");//设置编码速率 01 4/5
	delay_ms(1000);
	lora_dispalyRead();
	result = Lora_send_command("AT+MODE=00\r\n");//设置调制方式 00  lora模式
	delay_ms(1000);
	lora_dispalyRead();
	result = Lora_send_command("AT+SYNC=12\r\n");//设置同步字 默认0x12
	delay_ms(1000);
	lora_dispalyRead();
		                  //设置前导码 默认08
 	result = Lora_send_command("AT+TIQ=00\r\n");    //关闭模块发送载波反转
	delay_ms(1000);
	lora_dispalyRead();
	result = Lora_send_command("AT+RIQ=00\r\n");//关闭模块接收载波反转
	delay_ms(1000);
	lora_dispalyRead();
	result = Lora_send_command("AT+SIP=00\r\n");//设置序号及协议功能默认00
	delay_ms(1000);
	lora_dispalyRead();
	//result = Lora_send_command("AT+AK=00\r\n");//设置AES密钥 默认未配置密钥
	//result = Lora_send_command("AT+ACK=00\r\n");//设置模块应答，默认不应答
	//result = Lora_send_command("AT+LCP=00\r\n");//设置链路检测周期，默认不开启
	result = Lora_send_command("AT+LDR=00\r\n");//设置低速率优化
	delay_ms(1000);
	lora_dispalyRead();
	result = Lora_send_command("AT+PAR=00\r\n");//设置串口奇偶校验
	delay_ms(1000);
	lora_dispalyRead();
	result = Lora_send_command(lora_setfnbon); //设置退避策略
	delay_ms(1000);
	lora_dispalyRead();
	result = Lora_send_command(lora_ntn_cfg);   //查询配置后的信息
  delay_ms(3000);
	lora_dispalyRead();
	return result;
}	










//void  lora_dispalyRead1(){
//			// buffer2[wptr2] = 0;
////	     int i;
//		//while(buffer2[0]){
//	  printf("lora_read1:%s",buffer2);
//	    // printf("\r\n");
//			wptr2 = 0;
//		memset(buffer2,0,sizeof(buffer2));
////			for(i=0;i<512;i++){
////				 buffer2[i]=0;	
////			 }	
//	//	}
//}	