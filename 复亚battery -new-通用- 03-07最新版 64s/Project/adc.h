#ifndef __ADC_H_
#define __ADC_H_
#include "source.h"

#ifdef	__cplusplus
extern "C" {
#endif

void ADC_read();  //动态标定
void ADC_read_stand(); //强制标定4.88V
uint16_t bandgap_voltage();
	uint16_t adc_read_channel(uint8_t channel);
	static void _wait_conversion();
	int getBatteryCap(float volt);
	void save_battery_percentage_to_power(uint8_t channel);
	void get_channel_power(uint8_t channel);
	//电流转化
void Convert_external_current(void);
	//电压转化
void Convert_external_voltage(void);
	//飞机开机电压检测
void Boot_voltage (void);
//获取电压标定值
void calibrate_adc_vref();
	//200ms关闭测试电压
void Close_power_get_5(void);
	
//电压转化 关机不充电
void Convert_external_voltage(void);
//关机充电
void Power_off_charging_voltage(void);
//开机充电
void Power_on_charging_voltage(void);
//开机不充电
void Power_on_nocharging_voltage(void);
//通道5专用
void get_channel_power_5(uint8_t channel);	


//标定开机不充电 4 
void Power_on_nocharging_voltage_stand(float adc);
#ifdef	__cplusplus
}
#endif
#endif
