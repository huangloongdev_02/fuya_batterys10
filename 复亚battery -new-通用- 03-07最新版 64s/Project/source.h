#ifndef __SOURCE_H_
#define __SOURCE_H_

#define int8_t      char
#define uint8_t     unsigned char
#define uchar       unsigned char

#define uint16_t    unsigned int
#define size_t 			unsigned int 
#define int16_t     signed int

#define	uint32_t	unsigned long
#define	int32_t		signed long
#define size_t    unsigned int

#define DEBUGPRINT      1
#define NODEBUGPRINT    0

#define	FALSE	0
#define	TRUE	1

#define	OK		0
#define	ERROR	1

#define	DIR		P06
#define	STEP  P07


#define I2C_MODE_HW 1
#define I2C_MODE_SW 2
#define I2C_MODE I2C_MODE_SW

#define DOOR_MODE_MAGNET 1
#define DOOR_MODE_SWITCH 2
#define DOOR_MODE DOOR_MODE_SWITCH

#define DTLS_NO_DTLS 0
#define DTLS_WITH_PREDEFINED_PSK 1


#define test12 0
#define test13  1

//****************************************
//定时器1作为 串口1时钟输出频率
//****************************************
#define FOSC            11059200UL      
#define BRT             (65536 - FOSC / 9600 / 4)
#define BRT1             (65536 - FOSC / 9600 / 4)
	
//****************************************
// 定义MPU6050内部地址
//****************************************



//****************************************
// lora部分
//****************************************
#define LORA_MAX_BUFFER_LENGTH 1024 //
#define LORA_MAX_DATA_LENGTH 100

#define LORA_AT_PIN        GET_PIN(B, 13)
#define LORA_POWER_PIN        GET_PIN(B, 4)

//eeprom 部分
//#define WT_12M          0x83
#define WT_30M          0x80
#define WT_24M          0x81
#define WT_20M          0x82
#define WT_12M          0x83
#define WT_6M           0x84
#define WT_3M           0x85
#define WT_2M           0x86
#define WT_1M           0x87

//#ifndef  O1 P34 
//#define  O1 P34 
#define		O1			P34 
//#define		P27			P27 
//
//sfr     AUXR        =   0x8e;
//串口1使用寄存器
extern bit busy;
extern char wptr;
extern char rptr;
extern uchar buffer[16];

//串口2使用寄存器
extern bit busy2;
extern uint16_t wptr2;
extern char rptr2;
extern int8_t buffer2[256];

//串口3使用寄存器
extern char wptr3;
extern char rptr3;
extern char buffer3[16];
extern bit busy3;

//串口3使用寄存器
extern char wptr4;
extern char rptr4;
extern uchar buffer4[256];
extern bit busy4;

///* tmc262 register */
//#define		REG_DRVCTRL			0x00000000   //驱动控制寄存器
//#define		REG_CHOPCONF		0x00080000   //斩波设置寄存器
//#define		REG_SMARTEN			0x000A0000   //步进优化设置寄存器
//#define		REG_SGCSCONF		0x000C0000   //保护和诊断设置寄存器
//#define		REG_DRVCONF			0x000E0000   //驱动器设置寄存器

///* register value */
//#define		MICROSTEP_256		0x00
//#define		MICROSTEP_128		0x01
//#define		MICROSTEP_64		0x02
//#define		MICROSTEP_32		0x03
//#define		MICROSTEP_16		0x04
//#define		MICROSTEP_8			0x05
//#define		MICROSTEP_4			0x06
//#define		MICROSTEP_2			0x07
//#define		MICROSTEP_1			0x08

/* 默认配置 */
#define		SCG_DEFAULT			0x10000

#define		PRINT_CHOSE			1    

//spi使用寄存器
//extern  sbit    SS      ;
extern volatile bit busy_spi1;
extern uchar Send_complete_flag_spi;
extern uchar Spi_receive_buf[20];
extern uchar spi_getptr;
extern uint8_t spi_get_data[20];

extern uint8_t spi_del_flag;

extern int32_t Xactual_value;
extern uint32_t VMax_value;
extern uint8_t cod_add;

#define SS  P12
#define ENN  P44
extern char adc_date1;
extern char adc_date2;
extern float adc_num;
extern float adc_result;
extern float adc_result_cmpere;
extern float adc_vref_one;

extern uint16_t  ctime ;
extern uint16_t  ctime_1s ;
extern uint16_t  measure_time;
extern uint16_t  send_important_ctime_1s;

extern uchar   start_1;
extern uchar   start_2;
extern uchar  eeprom_read;
extern uchar  eeprom_read1;
extern uchar  eeprom_read2;
extern uchar  eeprom_read3;
extern uchar  eeprom_read4;

extern float mv_transf_v;
extern float write_acc_transf_standard;
extern float usual_v ;
extern float usual_acc ;
extern uint16_t mid_mv;
extern uint16_t mid_acc; 
		
extern uchar  eeprom_read5;
extern uchar  eeprom_read6;
extern uchar  eeprom_read7;
extern uchar  eeprom_read8;

extern uchar   meassage_num;
extern uchar   power;
extern uchar   state_data;
extern uchar   reserved1;
extern uchar   reserved2;
extern uchar   reserved3;
extern uchar   reserved4;
extern uchar   end_1;
extern uchar   end_2;

extern uchar led_speed;
extern uint16_t battery_percentage;

extern char  ntn_flag;

//新增变量
extern uchar   Uart2_getcom;
extern uchar   Uart2_get_data[256];

//新增变量
extern uchar   Uart4_getcom;
extern uchar   Uart4_get_data[256];
extern uchar   Uart4_send_data[15];
extern uint16_t   current_voltage;
extern uchar   current_power;
extern uchar   charging_time;
extern uchar   uart4_id;
extern uchar   current_state;
extern uchar   current_state_bits;
//extern enum  Chose_Mode;
extern uchar   inspection_data ;
extern uchar   get_inspection_data;
extern float   current_current;
extern float   Bo_voltage; //飞机启动后输出电压
extern uchar   Open_flag; //飞机开启标志
extern uchar   Dial_switch; //拨码选择打开
extern uchar   Power_compare;//用来过滤电量百分比的值
extern float	 v_sum;
extern uchar   Trans_chose;//上传是否需要开启
extern uchar   Trans_times;//上传周期设置
extern uint16_t __vref;
extern int num_tran_time;

extern uchar   Charging_time_limit;
//extern int  ctime ;
//extern int  ctime_1s ;

#include "STC8.h"
#include "math.h"
#include <stdio.h>
#include <string.h>
#include "intrins.h"
#include "uart.h"
#include "delay.h"
#include "iic.h"
#include "time3.h"
#include "tinit.h"
#include "uart2.h"
#include "lora.h"
#include "eeprom.h"
#include "adc.h"
#include "tmc262.h"
#include "uart4.h"

#endif
